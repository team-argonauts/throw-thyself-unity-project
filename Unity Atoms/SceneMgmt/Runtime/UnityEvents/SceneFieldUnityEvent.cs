using System;
using UltEvents;
using UnityAtoms.SceneMgmt;

namespace UnityAtoms.SceneMgmt
{
    /// <summary>
    /// None generic Unity Event of type `SceneField`. Inherits from `UltEvent&lt;SceneField&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SceneFieldUltEvent : UltEvent<SceneField> { }
}
