using System;
using UltEvents;
using UnityAtoms.SceneMgmt;

namespace UnityAtoms.SceneMgmt
{
    /// <summary>
    /// None generic Unity Event of type `SceneFieldPair`. Inherits from `UltEvent&lt;SceneFieldPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SceneFieldPairUltEvent : UltEvent<SceneFieldPair> { }
}
