using System;
using UltEvents;
using UnityAtoms.SceneMgmt;

namespace UnityAtoms.SceneMgmt
{
    /// <summary>
    /// None generic Unity Event x 2 of type `SceneField`. Inherits from `UltEvent&lt;SceneField, SceneField&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SceneFieldSceneFieldUltEvent : UltEvent<SceneField, SceneField> { }
}
