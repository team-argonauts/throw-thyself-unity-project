using System;
using UltEvents;
using UnityAtoms.Mobile;

namespace UnityAtoms.Mobile
{
    /// <summary>
    /// None generic Unity Event of type `TouchUserInput`. Inherits from `UltEvent&lt;TouchUserInput&gt;`.
    /// </summary>
    [Serializable]
    public sealed class TouchUserInputUltEvent : UltEvent<TouchUserInput> { }
}
