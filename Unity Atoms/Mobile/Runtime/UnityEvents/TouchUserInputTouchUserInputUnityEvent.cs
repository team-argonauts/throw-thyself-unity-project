using System;
using UltEvents;
using UnityAtoms.Mobile;

namespace UnityAtoms.Mobile
{
    /// <summary>
    /// None generic Unity Event x 2 of type `TouchUserInput`. Inherits from `UltEvent&lt;TouchUserInput, TouchUserInput&gt;`.
    /// </summary>
    [Serializable]
    public sealed class TouchUserInputTouchUserInputUltEvent : UltEvent<TouchUserInput, TouchUserInput> { }
}
