using System;
using UltEvents;
using UnityAtoms.Mobile;

namespace UnityAtoms.Mobile
{
    /// <summary>
    /// None generic Unity Event of type `TouchUserInputPair`. Inherits from `UltEvent&lt;TouchUserInputPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class TouchUserInputPairUltEvent : UltEvent<TouchUserInputPair> { }
}
