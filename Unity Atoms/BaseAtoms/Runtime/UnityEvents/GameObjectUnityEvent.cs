using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `GameObject`. Inherits from `UltEvent&lt;GameObject&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GameObjectUltEvent : UltEvent<GameObject> { }
}
