using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `bool`. Inherits from `UltEvent&lt;bool&gt;`.
    /// </summary>
    [Serializable]
    public sealed class BoolUltEvent : UltEvent<bool> { }
}
