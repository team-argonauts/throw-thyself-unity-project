using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `GameObjectPair`. Inherits from `UltEvent&lt;GameObjectPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GameObjectPairUltEvent : UltEvent<GameObjectPair> { }
}
