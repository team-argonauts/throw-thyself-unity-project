using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Vector3`. Inherits from `UltEvent&lt;Vector3&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Vector3UltEvent : UltEvent<Vector3> { }
}
