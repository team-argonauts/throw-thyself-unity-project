using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Collider`. Inherits from `UltEvent&lt;Collider&gt;`.
    /// </summary>
    [Serializable]
    public sealed class ColliderUltEvent : UltEvent<Collider> { }
}
