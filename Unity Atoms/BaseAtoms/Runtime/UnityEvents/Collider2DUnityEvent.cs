using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Collider2D`. Inherits from `UltEvent&lt;Collider2D&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Collider2DUltEvent : UltEvent<Collider2D> { }
}
