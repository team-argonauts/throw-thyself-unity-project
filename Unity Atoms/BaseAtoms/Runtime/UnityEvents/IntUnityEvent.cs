using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `int`. Inherits from `UltEvent&lt;int&gt;`.
    /// </summary>
    [Serializable]
    public sealed class IntUltEvent : UltEvent<int> { }
}
