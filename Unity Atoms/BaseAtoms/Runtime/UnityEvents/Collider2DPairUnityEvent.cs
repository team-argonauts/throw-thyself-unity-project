using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Collider2DPair`. Inherits from `UltEvent&lt;Collider2DPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Collider2DPairUltEvent : UltEvent<Collider2DPair> { }
}
