using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Vector2Pair`. Inherits from `UltEvent&lt;Vector2Pair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Vector2PairUltEvent : UltEvent<Vector2Pair> { }
}
