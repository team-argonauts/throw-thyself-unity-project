using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Color`. Inherits from `UltEvent&lt;Color&gt;`.
    /// </summary>
    [Serializable]
    public sealed class ColorUltEvent : UltEvent<Color> { }
}
