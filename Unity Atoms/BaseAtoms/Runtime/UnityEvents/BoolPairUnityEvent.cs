using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `BoolPair`. Inherits from `UltEvent&lt;BoolPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class BoolPairUltEvent : UltEvent<BoolPair> { }
}
