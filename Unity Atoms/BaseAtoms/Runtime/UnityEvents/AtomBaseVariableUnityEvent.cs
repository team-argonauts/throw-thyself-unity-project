using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `AtomBaseVariable`. Inherits from `UltEvent&lt;AtomBaseVariable&gt;`.
    /// </summary>
    [Serializable]
    public sealed class AtomBaseVariableUltEvent : UltEvent<AtomBaseVariable> { }
}
