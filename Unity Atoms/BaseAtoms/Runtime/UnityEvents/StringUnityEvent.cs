using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `string`. Inherits from `UltEvent&lt;string&gt;`.
    /// </summary>
    [Serializable]
    public sealed class StringUltEvent : UltEvent<string> { }
}
