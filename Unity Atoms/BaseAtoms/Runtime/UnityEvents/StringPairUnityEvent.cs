using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `StringPair`. Inherits from `UltEvent&lt;StringPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class StringPairUltEvent : UltEvent<StringPair> { }
}
