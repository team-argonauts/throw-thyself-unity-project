using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `FloatPair`. Inherits from `UltEvent&lt;FloatPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class FloatPairUltEvent : UltEvent<FloatPair> { }
}
