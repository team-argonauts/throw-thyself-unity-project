using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `ColliderPair`. Inherits from `UltEvent&lt;ColliderPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class ColliderPairUltEvent : UltEvent<ColliderPair> { }
}
