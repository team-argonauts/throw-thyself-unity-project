using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `IntPair`. Inherits from `UltEvent&lt;IntPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class IntPairUltEvent : UltEvent<IntPair> { }
}
