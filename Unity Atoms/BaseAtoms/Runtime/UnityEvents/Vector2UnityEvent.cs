using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Vector2`. Inherits from `UltEvent&lt;Vector2&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Vector2UltEvent : UltEvent<Vector2> { }
}
