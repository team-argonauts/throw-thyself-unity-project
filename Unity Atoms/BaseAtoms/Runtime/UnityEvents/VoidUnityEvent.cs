using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Void`. Inherits from `UltEvent&lt;Void&gt;`.
    /// </summary>
    [Serializable]
    public sealed class VoidUltEvent : UltEvent<Void> { }
}
