using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Vector3Pair`. Inherits from `UltEvent&lt;Vector3Pair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Vector3PairUltEvent : UltEvent<Vector3Pair> { }
}
