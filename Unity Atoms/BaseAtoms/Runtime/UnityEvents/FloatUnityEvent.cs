using System;
using UltEvents;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `float`. Inherits from `UltEvent&lt;float&gt;`.
    /// </summary>
    [Serializable]
    public sealed class FloatUltEvent : UltEvent<float> { }
}
