using System;
using UltEvents;
using UnityEngine;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// None generic Unity Event of type `ColorPair`. Inherits from `UltEvent&lt;ColorPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class ColorPairUltEvent : UltEvent<ColorPair> { }
}
