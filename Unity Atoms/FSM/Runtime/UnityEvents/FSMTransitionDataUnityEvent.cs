using System;
using UltEvents;

namespace UnityAtoms.FSM
{
    /// <summary>
    /// None generic Unity Event of type `FSMTransitionData`. Inherits from `UltEvent&lt;FSMTransitionData&gt;`.
    /// </summary>
    [Serializable]
    public sealed class FSMTransitionDataUltEvent : UltEvent<FSMTransitionData> { }
}
