using System;
using UltEvents;
using UnityAtoms.MonoHooks;

namespace UnityAtoms.MonoHooks
{
    /// <summary>
    /// None generic Unity Event of type `ColliderGameObject`. Inherits from `UltEvent&lt;ColliderGameObject&gt;`.
    /// </summary>
    [Serializable]
    public sealed class ColliderGameObjectUltEvent : UltEvent<ColliderGameObject> { }
}
