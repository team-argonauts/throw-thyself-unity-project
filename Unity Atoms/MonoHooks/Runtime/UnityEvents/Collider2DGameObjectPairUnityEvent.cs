using System;
using UltEvents;
using UnityAtoms.MonoHooks;

namespace UnityAtoms.MonoHooks
{
    /// <summary>
    /// None generic Unity Event of type `Collider2DGameObjectPair`. Inherits from `UltEvent&lt;Collider2DGameObjectPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Collider2DGameObjectPairUltEvent : UltEvent<Collider2DGameObjectPair> { }
}
