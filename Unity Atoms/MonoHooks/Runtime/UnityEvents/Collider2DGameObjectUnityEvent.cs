using System;
using UltEvents;
using UnityAtoms.MonoHooks;

namespace UnityAtoms.MonoHooks
{
    /// <summary>
    /// None generic Unity Event of type `Collider2DGameObject`. Inherits from `UltEvent&lt;Collider2DGameObject&gt;`.
    /// </summary>
    [Serializable]
    public sealed class Collider2DGameObjectUltEvent : UltEvent<Collider2DGameObject> { }
}
