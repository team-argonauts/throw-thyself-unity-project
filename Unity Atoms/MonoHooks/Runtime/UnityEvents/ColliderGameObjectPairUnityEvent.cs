using System;
using UltEvents;
using UnityAtoms.MonoHooks;

namespace UnityAtoms.MonoHooks
{
    /// <summary>
    /// None generic Unity Event of type `ColliderGameObjectPair`. Inherits from `UltEvent&lt;ColliderGameObjectPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class ColliderGameObjectPairUltEvent : UltEvent<ColliderGameObjectPair> { }
}
