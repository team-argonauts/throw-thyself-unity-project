﻿using System.Linq;
using UnityAtoms;
using UnityEngine;

/// <summary> Tries to retrieve a Player and holds it's corresponding info. </summary>
public class PlayerInfoHolder : MonoBehaviour
{
    [SerializeField] private PlayerInfoValueList _playerInfoList = default;

    public PlayerInfo PlayerInfo { get; private set; } = default;

    public Color PlayerColor => PlayerInfo.PlayerColor;
    public Sprite PlayerPortraitSprite => PlayerInfo.PlayerPortraitSprite;


    private void Start()
    {
        Player parentPlayer = gameObject.GetComponentRelative<Player>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT, false);
        if (parentPlayer)
            PlayerInfo = _playerInfoList[parentPlayer.PlayerIndex];
    }
}
