﻿using UnityEngine;
using UnityEngine.Assertions;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// Adds a Player to a Player Value List on OnEnable and removes it on OnDisable.
    /// </summary>
    [AddComponentMenu("Unity Atoms/MonoBehaviour Helpers/Sync Player To List")]
    [EditorIcon("atom-icon-delicate")]
    public class SyncPlayerToList : MonoBehaviour
    {
        [SerializeField] private PlayerReference _player = default;
        [SerializeField] private PlayerValueList _playerList = default;

        void OnEnable()
        {
            Assert.IsNotNull(_playerList);
            _player.Value.PlayerIndex = _playerList.Count;
            _playerList.Add(_player);
        }

        private void OnDisable()
        {
            Assert.IsNotNull(_playerList);
            _playerList.Remove(_player);
        }
    }
}
