﻿using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityAtoms;


public class GodSwapper : MonoBehaviour
{
    [SerializeField] private GameObjectReference _godPrefab = default;
    [SerializeField] private Vector2Reference _spawnOffset = default;
    [SerializeField] private GodValueList _godList = default;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Player player;
        if ((player = other.GetComponent<Player>()) == null &&
            (player = other.GetComponentInParent<Player>()) == null)
            return;

        if (PlayerHelper.PlayerAlive(_godList, player))
            player.SwapGod(_godPrefab, (Vector2)transform.position + _spawnOffset.Value);
    }
}
