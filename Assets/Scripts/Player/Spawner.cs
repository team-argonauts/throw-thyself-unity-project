﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityAtoms;
using UnityAtoms.BaseAtoms;


public class Spawner : MonoBehaviour
{
    [SerializeField] private Vector2Reference[] _spawnPositions = default;
    [SerializeField] private PlayerValueList _playerList = default;
    [SerializeField] private GodValueList _godList = default;

    [SerializeField] private FloatReference _safeZoneRadius = default;

    [SerializeField] private FloatReference _spawnExplosionForce = default;
    [SerializeField] private FloatReference _spawnExplosionRadius = default;
    [SerializeField] private FloatReference _upwardsModifier = default;


    public void Spawn(God god, float delay)
    {
        Player matchingPlayer = PlayerHelper.GetPlayerFromGod(god, _playerList);
        if (matchingPlayer)
            StartCoroutine(SpawnDelayed(matchingPlayer, delay));
    }

    private IEnumerator SpawnDelayed(Player Player, float delay)
    {
        God[] explosionTargets = _godList.ToArray();
        
        yield return new WaitForSeconds(delay);


        Vector2 spawnPosition = GetSafeSpawnPosition();
        Player.Spawn(spawnPosition);

        foreach (God explosionTarget in explosionTargets)
        {
            if (explosionTarget.WeaponBody != null)
                explosionTarget.WeaponBody.AddVelocityChangeExplosionForce(_spawnExplosionForce.Value, spawnPosition, _spawnExplosionRadius.Value, _upwardsModifier.Value);
            if (explosionTarget.RagdollExploder != null)
                explosionTarget.RagdollExploder.AddPointExplosionForce(_spawnExplosionForce.Value, spawnPosition, _spawnExplosionRadius.Value, _upwardsModifier.Value);
        }
    }

    private Vector2 GetSafeSpawnPosition()
    {
        int spawnIndex = Random.Range(0, _spawnPositions.Length);
        int iterations = 0;

        do { spawnIndex = (spawnIndex + 1) % _spawnPositions.Length; }
        while (!SpawnPointIsSafe(_spawnPositions[spawnIndex]) && ++iterations < _spawnPositions.Length);

        return _spawnPositions[spawnIndex];
    }

    private bool SpawnPointIsSafe(Vector2 spawnPoint)
    {
        foreach (God god in _godList)
            if (Vector2.Distance(god.WeaponBody.position, spawnPoint) < _safeZoneRadius)
                return false;

        return true;
    }
}
