﻿using UltEvents;
using UnityEngine;



/// <summary> GameObject: godObject </summary>
[System.Serializable] public class GodUltEvent : UltEvent<GameObject> { }



public class Player : MonoBehaviour
{
    [SerializeField] private GameObject _godPrefab = default;

    [SerializeField] private GodUltEvent OnGodSpawned = default;
    [SerializeField] private UltEvent OnGodDespawned = default;
    [SerializeField] private GodUltEvent OnGodSwapped = default;


    public int PlayerIndex { get; set; } = default;
    public GameObject InstantiatedGod { get; private set; } = default;


    private void Awake() => Spawn(Vector2.zero);

    public void Spawn(Vector2 position)
    {
        Despawn();

        if (_godPrefab == null)
            InstantiatedGod = null;
        else
            InstantiatedGod = Instantiate(_godPrefab, position, Quaternion.identity, transform);

        OnGodSpawned?.Invoke(InstantiatedGod);
    }

    public void Despawn()
    {
        if (InstantiatedGod != null)
        {
            Destroy(InstantiatedGod);
            InstantiatedGod = null;
            OnGodDespawned?.Invoke();
        }
    }

    public void SwapGod(GameObject newGodPrefab, Vector2 swapPosition)
    {
        if (_godPrefab == newGodPrefab)
            return;

        _godPrefab = newGodPrefab;
        Spawn(swapPosition);
        OnGodSwapped?.Invoke(InstantiatedGod);
    }
}
