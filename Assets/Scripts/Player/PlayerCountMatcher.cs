﻿using UltEvents;
using UnityAtoms;
using UnityEngine;


public class PlayerCountMatcher : MonoBehaviour
{
    [SerializeField] private PlayerValueList _playerList = default;

    [SerializeField] private UltEvent OnNoneMatch = default;
    [SerializeField] private UltEvent OnMatch = default;
    [SerializeField] private UltEvent OnUnMatch = default;
    [SerializeField] private UltEvent OnAllMatch = default;

    private int _playerCount = 0;
    private int _previousPlayerCount = 0;

    private bool DidUnMatch => _playerCount < _previousPlayerCount;
    private bool DidMatch => _playerCount > _previousPlayerCount;
    private bool DidMatchAll => _playerCount >= _playerList.Count && _playerCount > 0;
    private bool DidMatchNone => _playerCount == 0;



    private void Start()
    {
        DoMatchEvent();
        _playerList.Added.Register(OnPlayerListIncreased);
        _playerList.Removed.Register(OnPlayerListDecreased);
    }

    private void OnPlayerListIncreased()
    {
        OnUnMatch?.Invoke();
        DoMatchEvent();
    }
    private void OnPlayerListDecreased()
    {
        OnMatch?.Invoke();
        DoMatchEvent();
    }



    public void AddPlayerCount()
    {
        _playerCount++;
        DoMatchEvent();
        _previousPlayerCount = _playerCount;
    }

    public void RemovePlayerCount()
    {
        _playerCount--;
        DoMatchEvent();
        _previousPlayerCount = _playerCount;
    }

    private void DoMatchEvent()
    {
        if (DidMatch)
            OnMatch?.Invoke();
        
        if (DidUnMatch)
            OnUnMatch?.Invoke();

        if (DidMatchAll)
            OnAllMatch?.Invoke();

        if (DidMatchNone)
            OnNoneMatch?.Invoke();
    }
}
