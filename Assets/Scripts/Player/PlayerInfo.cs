﻿using System;
using UnityEngine;


[Serializable]
public struct PlayerInfo
{
    [SerializeField] private Color _playerColor;
    [SerializeField] private Sprite _playerPortraitSprite;

    public Color PlayerColor => _playerColor;
    public Sprite PlayerPortraitSprite => _playerPortraitSprite;


    public PlayerInfo(Color playerColor, Sprite playerPortraitSprite)
    {
        _playerColor = playerColor;
        _playerPortraitSprite = playerPortraitSprite;
    }
}
