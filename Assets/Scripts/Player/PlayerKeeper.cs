﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerKeeper : MonoBehaviour
{
    [SerializeField] private int[] _invalidScenes = default;
    private readonly List<Player> _playersToKeep = new List<Player>();


    private void Awake() => SceneManager.sceneLoaded += OnSceneChanged;


    private void OnSceneChanged(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (_invalidScenes.Contains(scene.buildIndex))
        {
            foreach (Player player in _playersToKeep.ToArray())
            {
                if (player == null)
                {
                    _playersToKeep.Remove(player);
                    continue;
                }

                UnKeep(player);
                Destroy(player.gameObject);
            }
        }

        // @TODO: TEMPORARY
        foreach (Player player in _playersToKeep)
        {
            player.InstantiatedGod.GetComponentInChildren<Killable>().Kill(Vector2.zero, Vector2.zero);
            player.Despawn();
        }
    }

    public void Keep(Player playerToKeep)
    {
        if (playerToKeep == null)
            return;
        
        DontDestroyOnLoad(playerToKeep.gameObject);
        
        if (!_playersToKeep.Contains(playerToKeep))
            _playersToKeep.Add(playerToKeep);
    }

    public void UnKeep(Player playerToUnKeep)
    {
        if (playerToUnKeep == null)
            return;

        SceneManager.MoveGameObjectToScene(playerToUnKeep.gameObject, SceneManager.GetActiveScene());
        
        if (_playersToKeep.Contains(playerToUnKeep))
            _playersToKeep.Remove(playerToUnKeep);
    }
}
