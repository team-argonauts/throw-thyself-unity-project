﻿using UnityAtoms.BaseAtoms;
using UnityAtoms.Tags;
using UnityEngine;


public class DeactivateTaggedGameObjects : MonoBehaviour
{
    [SerializeField] private StringReference[] _tags = default;


    private void Start() => DeactivateTagged();

    public void DeactivateTagged()
    {
        AtomTags[] atomTags = FindObjectsOfType<AtomTags>();
        foreach (AtomTags atomTag in atomTags)
            foreach (StringReference tag in _tags)
                if (atomTag.HasTag(tag.Value))
                    atomTag.gameObject.SetActive(false);
    }
}
