﻿using UnityEngine;
using UnityEngine.InputSystem;
using UltEvents;


[System.Serializable] public class CallbackContextUltEvent : UltEvent<InputAction.CallbackContext> { }


public class InputActionReciever : MonoBehaviour
{
    [SerializeField] private InputActionReference _inputActionReference = default;
    [SerializeField] private CallbackContextUltEvent _OnActionPerformed = default;

    private void OnEnable()
    {
        _inputActionReference.action.Enable();
        _inputActionReference.action.performed += _OnActionPerformed.Invoke;
    }
    private void OnDisable()
    {
        _inputActionReference.action.performed -= _OnActionPerformed.Invoke;
        _inputActionReference.action.Disable();
    }
}
