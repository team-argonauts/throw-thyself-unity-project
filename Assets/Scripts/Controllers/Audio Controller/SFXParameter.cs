﻿using System;
using UnityEngine;


[Serializable]
public struct SFXParameter
{
    [SerializeField] private AudioClip _audioClip;
    [SerializeField] private float _volume;

    public AudioClip AudioClip => _audioClip;
    public float Volume => _volume;

    public SFXParameter(AudioClip audioClip, float volume)
    {
        _audioClip = audioClip;
        _volume = volume;
    }
}
