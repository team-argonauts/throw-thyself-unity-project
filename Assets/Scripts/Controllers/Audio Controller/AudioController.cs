﻿using UnityEngine;


public static class AudioController
{
    public static void PlaySFX(SFXParameter sfxParameter, Transform transform)
    {
        if (sfxParameter.AudioClip == null)
            return;

        Vector2 playPosition = Vector2.zero;
        if (transform != null)
            playPosition = transform.position;

        AudioSource.PlayClipAtPoint(sfxParameter.AudioClip, playPosition, sfxParameter.Volume);
    }

    public static void PlaySFX(SFXParameter sfxParameter, Vector2 playPosition)
    {
        if (sfxParameter.AudioClip == null)
            return;

        AudioSource.PlayClipAtPoint(sfxParameter.AudioClip, playPosition, sfxParameter.Volume);
    }
}
