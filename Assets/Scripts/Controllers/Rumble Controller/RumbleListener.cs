﻿using UnityEngine;
using UnityEngine.InputSystem;


public class RumbleListener : MonoBehaviour
{
    [SerializeField] private PlayerInput _playerInput = default;

    private Gamepad _gamepad = default;
    private float _highestLowA = default;
    private float _highestHighA = default;


    private void Awake() => _gamepad = GamepadHelper.GetGamepadFromPlayerInput(_playerInput);
    private void Start() => RegisterSources();

    public void RegisterSources()
    {
        RumbleSource[] rumbleSources = gameObject.GetComponentsRelative<RumbleSource>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, true);
        foreach (RumbleSource rumbleSource in rumbleSources)
            rumbleSource.OnReportRumble += TrySetRumble;
    }
    

    private void Update()
    {
        if (_gamepad == null)
            return;

        _gamepad.SetMotorSpeeds(_highestLowA, _highestHighA);
        _highestLowA = 0.0f;
        _highestHighA = 0.0f;
    }


    private void TrySetRumble(float lowA, float highA)
    {
        _highestLowA = Mathf.Max(lowA, _highestLowA);
        _highestHighA = Mathf.Max(highA, _highestHighA);
    }

    private void OnApplicationQuit()
    {
        if (_gamepad == null)
            return;

        _gamepad.SetMotorSpeeds(0.0f, 0.0f);
    }
}