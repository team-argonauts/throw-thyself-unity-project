﻿using System;
using UnityEngine;


[Serializable]
public struct RumblePatternParameter
{
    [SerializeField] private float _lowA;
    [SerializeField] private float _highA;
    [SerializeField] private Func<float, float> _pattern;
    [SerializeField] private float _duration;

    public float LowA => _lowA;
    public float HighA => _highA;
    public Func<float, float> Pattern => _pattern;
    public float Duration => _duration;


    public RumblePatternParameter(float lowA, float highA, Func<float, float> pattern, float duration)
    {
        _lowA = lowA;
        _highA = highA;
        _pattern = pattern;
        _duration = duration;
    }
}