﻿using System;
using UnityEngine;


[Serializable]
public struct RumbleParameter
{
    [SerializeField] private float _lowA;
    [SerializeField] private float _highA;

    public float LowA => _lowA;
    public float HighA => _highA;

    public RumbleParameter(float lowA, float highA)
    {
        _lowA = lowA;
        _highA = highA;
    }
}