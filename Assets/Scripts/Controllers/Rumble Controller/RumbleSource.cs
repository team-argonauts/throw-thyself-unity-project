﻿using System;
using System.Collections;
using UnityEngine;


public class RumbleSource : MonoBehaviour
{
    public delegate void RumbleReport(float lowA, float highA);
    public RumbleReport OnReportRumble;


    public void StartRumblePattern(RumblePatternParameter patternParameter) => StartCoroutine(DoRumblePattern(patternParameter.LowA, patternParameter.HighA, TweenScaleFunctions.Linear, patternParameter.Duration));

    private IEnumerator DoRumblePattern(float lowA, float highA, Func<float, float> rumblePattern, float duration)
    {
        TimeTracker rumbleTimer = new TimeTracker();
        rumbleTimer.Set(duration);
        WaitForEndOfFrame waitForEndOfFrame = new WaitForEndOfFrame();

        while (!rumbleTimer.Stopped)
        {
            float rumbleValue = rumblePattern.Invoke(rumbleTimer.Progress);
            OnReportRumble?.Invoke(lowA * rumbleValue, highA * rumbleValue);
            yield return waitForEndOfFrame;
        }
    }

    public void SetScaledRumble(float scalePercent, RumbleParameter rumbleParameter) => OnReportRumble?.Invoke(scalePercent * rumbleParameter.LowA, scalePercent * rumbleParameter.HighA);

    public void SetRumble(RumbleParameter rumbleParameter) => OnReportRumble?.Invoke(rumbleParameter.LowA, rumbleParameter.HighA);
}