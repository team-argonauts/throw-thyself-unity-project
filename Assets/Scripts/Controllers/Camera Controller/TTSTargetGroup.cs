﻿using UnityEngine;
using UnityEngine.Assertions;
using Cinemachine;
using UnityAtoms.BaseAtoms;


public class TTSTargetGroup : CinemachineTargetGroup
{
    [SerializeField] private FloatReference _weight = default;
    [SerializeField] private FloatReference _radius = default;

    public void AddMember(God god)
    {
        if (god.GodObject != null)
            AddMember(god.FollowTarget, _weight.Value, _radius.Value);
    }
    public void RemoveMember(God god)
    {
        if (god.GodObject != null)
            RemoveMember(god.FollowTarget);
    }
}
