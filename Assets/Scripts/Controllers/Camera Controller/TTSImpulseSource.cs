﻿using UnityEngine;
using Cinemachine;


public class TTSImpulseSource : CinemachineImpulseSource
{
    public void GenerateImpulse(float impulseScalar, Vector2 velocity) => GenerateImpulse(impulseScalar * velocity);
    public void GenerateImpulseAt(Vector2 position, float impulseScalar, Vector2 velocity) => GenerateImpulseAt(position, impulseScalar * velocity);
}