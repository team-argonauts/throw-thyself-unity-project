﻿using UnityEngine;


public static class ParticleController
{
    public static void SpawnParticleLocal(GameObject particlePrefab, Transform parent) => Object.Instantiate(particlePrefab, parent);
    public static void SpawnParticleInWorld(GameObject particlePrefab, Vector2 position) => Object.Instantiate(particlePrefab, position, particlePrefab.transform.rotation);
    public static void SpawnParticleInWorld(GameObject particlePrefab, Vector2 position, Quaternion rotation) => Object.Instantiate(particlePrefab, position, rotation);
    public static void SpawnParticleInWorld(GameObject particlePrefab, Vector2 position, Vector2 normal) => Object.Instantiate(particlePrefab, position, Quaternion.Euler(normal));
    public static void SpawnParticleInWorld(GameObject particlePrefab, Transform positionRotation) => Object.Instantiate(particlePrefab, positionRotation.position, positionRotation.rotation);
}
