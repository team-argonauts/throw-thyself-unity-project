﻿using UnityAtoms.BaseAtoms;
using UnityEngine;


public class Spinner : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _spinBody = default;
    [SerializeField] private FloatReference _spinRate = default;

    private void FixedUpdate() => _spinBody.MoveRotation(_spinBody.rotation + _spinRate.Value * Time.fixedDeltaTime);
}