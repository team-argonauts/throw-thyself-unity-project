﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;


/// <summary> Vector2: bounceHitPosition, Vector2: bounceHitVelocity, Vector2: bounceHitNormal </summary>
[System.Serializable] public class BounceHitUltEvent : UltEvents.UltEvent<Vector2, Vector2, Vector2> { }

/// <summary> Vector2: bounceOffPosition, Vector2: bounceOffForce </summary>
[System.Serializable] public class BounceOffUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


[SelectionBase]
public class Bouncer : MonoBehaviour
{
    private struct BouncePoint
    {
        public Rigidbody2D BounceBody { get; private set; }
        public Bounceable Bounceable { get; private set; }
        public Vector2 BounceNormal { get; private set; }

        public BouncePoint(Rigidbody2D bounceBody, Vector2 bounceNormal)
        {
            BounceBody = bounceBody;
            if ((Bounceable = bounceBody.GetComponent<Bounceable>()) == null)
                Bounceable = bounceBody.GetComponentInChildren<Bounceable>();
            BounceNormal = bounceNormal;
        }
    }


    [SerializeField] private FloatReference _squishScalar = default;
    [SerializeField] private FloatReference _squishTime = default;
    [SerializeField] private FloatReference _bounceOffStrength = default;

    private readonly List<BouncePoint> _bouncePoints = new List<BouncePoint>();

    [SerializeField] private BounceHitUltEvent OnBounceHit = default;
    [SerializeField] private BounceOffUltEvent OnBounceOff = default;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.rigidbody == null)
            return;

        Vector2 collisionPoint = collision.GetAverageCollisionPoint();
        Vector2 collisionNormal = collision.GetAverageCollisionNormal();
        Vector2 localCollisionNormal = transform.InverseTransformDirection(collisionNormal);
        Vector2 projectedCollisionVelocity = collision.relativeVelocity * collisionNormal;
        Vector2 squishForce = localCollisionNormal * projectedCollisionVelocity.magnitude * collision.rigidbody.mass;

        StartCoroutine(SquishTowardsDirection(squishForce));
        BounceOffContacting(collision.rigidbody, squishForce);

        BouncePoint newBouncePoint = new BouncePoint(collision.rigidbody, collisionNormal);
        _bouncePoints.Add(newBouncePoint);

        OnBounceHit?.Invoke(collisionPoint, projectedCollisionVelocity, collisionNormal);
        if (newBouncePoint.Bounceable != null)
            newBouncePoint.Bounceable.BounceHit(collisionPoint, projectedCollisionVelocity, collisionNormal);
    }


    private IEnumerator SquishTowardsDirection(Vector2 squishForce)
    {
        Vector2 scaledSquishForce = _squishScalar.Value * squishForce;

        TimeTracker squishTimer = new TimeTracker();
        squishTimer.Set(_squishTime.Value);

        float initialScaleMagnitude = transform.localScale.magnitude;

        Vector2 previousScaleVector = Vector2.zero;
        Vector2 previousMovementVector = Vector2.zero;

        do
        {
            yield return null;

            float squishProgressValue = TweenScaleFunctions.UpDownPower(squishTimer.Progress, 4);
            float scaledSquishValue = squishProgressValue * initialScaleMagnitude;

            Vector2 scaleVector = scaledSquishValue *  -scaledSquishForce.Abs();
            Vector2 movementVector = scaledSquishValue *  scaledSquishForce / 2.0f;
                  
            Vector2 scaleChange = scaleVector - previousScaleVector;
            Vector2 movementChange = movementVector - previousMovementVector;

            transform.localScale += (Vector3)scaleChange;
            transform.Translate(movementChange);

            previousScaleVector = scaleVector;
            previousMovementVector = movementVector;

        } while (!squishTimer.Stopped);
    }


    private void BounceOffContacting(Rigidbody2D bounceInvoker, Vector2 squishForce)
    {
        foreach (BouncePoint bouncePoint in _bouncePoints.ToArray())
        {
            if (bouncePoint.BounceBody == bounceInvoker)
                continue;

            if (bouncePoint.BounceBody == null)
            {
                _bouncePoints.Remove(bouncePoint);
                continue;
            }

            Vector2 bounceForce = bouncePoint.BounceNormal * _bounceOffStrength.Value * squishForce.magnitude;
            bouncePoint.BounceBody.AddVelocityChangeForce(bounceForce);

            OnBounceOff?.Invoke(bouncePoint.BounceBody.position, bounceForce);
            if (bouncePoint.Bounceable != null)
                bouncePoint.Bounceable.BounceOff(bouncePoint.BounceBody.position, bounceForce);
        }
    }


    private void OnCollisionExit2D(Collision2D collision) => _bouncePoints.RemoveAll((bouncePoint) => bouncePoint.BounceBody == collision.rigidbody);
}
