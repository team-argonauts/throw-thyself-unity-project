﻿using UnityEngine;


public class KillerEnclosure : KillerBase
{
    private void OnTriggerExit2D(Collider2D other)
    {
        Vector2 killPosition = other.ClosestPoint(transform.position);
        Vector2 killDirection = ((Vector2)transform.position - killPosition).normalized;
        DoKill(other.gameObject, killPosition, killDirection);
    }
}
