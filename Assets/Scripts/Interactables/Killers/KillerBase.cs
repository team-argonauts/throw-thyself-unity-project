﻿using UnityAtoms.BaseAtoms;
using UnityEngine;


/// <summary> Vector2: killPosition, Vector2: killForce </summary>
[System.Serializable] public class KillUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public abstract class KillerBase : MonoBehaviour
{
    [SerializeField] private FloatReference _killForce = default;
    [SerializeField] private KillUltEvent OnKill = default;

    protected void DoKill(GameObject other, Vector2 killPosition, Vector2 killDirection)
    {
        Killable killable = other.GetComponentRelative<Killable>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT | eComponentRelativeType.CHILDREN, false);
        if (killable == null || !isActiveAndEnabled)
            return;

        killable.Kill(killPosition, _killForce.Value * killDirection);
        OnKill?.Invoke(killPosition, _killForce.Value * killDirection);
    }
}