﻿using UnityEngine;


[SelectionBase]
public class KillerSurface : KillerBase
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 killPosition = collision.GetAverageCollisionPoint();
        Vector2 killDirection = -collision.GetAverageCollisionNormal();
        DoKill(collision.gameObject, killPosition, killDirection);
    }
}
