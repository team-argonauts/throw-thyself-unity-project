﻿using UnityEngine;
using UnityAtoms.BaseAtoms;
using UltEvents;


public class Scroller : MonoBehaviour
{
    [SerializeField] private BoolReference _isPhysicalMovement = default;
    [SerializeField] private Transform _scrollTransform = default;
    [SerializeField] private Rigidbody2D _scrollBody = default;
    [SerializeField] private FloatReference _scrollTime = default;
    [SerializeField] private Vector2Reference _axis = default;

    private readonly TimeTracker _scrollTimer = new TimeTracker();
    private float _previousValue = default;

    [SerializeField] private UltEvent OnScrollReset = default;


    private void Start() => _scrollTimer.Set(_scrollTime);

    private void FixedUpdate()
    {
        float currentValue = TweenScaleFunctions.Linear(_scrollTimer.Progress);
        float changeValue = currentValue - Mathf.Abs(_previousValue);
        _previousValue = currentValue;

        Vector2 movementVector = changeValue * _axis.Value;
        TranslateScrollObject(movementVector);

        if (_scrollTimer.Stopped)
            ResetScroll();
    }

    private void ResetScroll()
    {
        OnScrollReset?.Invoke();

        _scrollTimer.Set(_scrollTime);
        SetScrollObjectPosition(_scrollBody.position - _axis.Value);
        _previousValue = 0.0f;
    }

    private void TranslateScrollObject(Vector2 movement)
    {
        if (_isPhysicalMovement)
            _scrollBody.MovePosition(_scrollBody.position + movement);
        else
            _scrollTransform.Translate(movement);
    }

    private void SetScrollObjectPosition(Vector2 position)
    {
        if (_isPhysicalMovement)
            _scrollBody.position = position;
        else
            _scrollTransform.position = position;
    }
}
