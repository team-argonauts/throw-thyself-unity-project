﻿using System;
using UnityEngine;
using UnityAtoms.BaseAtoms;


public class Looper : MonoBehaviour
{
    [Serializable]
    private class Loop
    {
        [SerializeField] private FloatReference _loopTime = default;
        [SerializeField] private FloatReference _delay = default;
        [SerializeField] private Vector2Reference _axis = default;

        private float _changeValue = default;
        private float _previousValue = default;

        public Vector2 MovementVector => _changeValue * _axis.Value;
        private float LoopIterationValue {
            get {
                float sine01NormaliseValue = 2.0f * Mathf.PI;
                float scaledTime = _loopTime == 0.0f ? 0.0f : Time.time / _loopTime.Value;
                return sine01NormaliseValue * (scaledTime + _delay.Value);
            }
        }


        public void Setup() => _previousValue = GetLoopTweenValue(0.0f);
        public void Update()
        {
            float currentValue = GetLoopTweenValue(LoopIterationValue);
            _changeValue = currentValue - _previousValue;
            _previousValue = currentValue;
        }

        private static float GetLoopTweenValue(float a) => (Mathf.Sin(a) + 1) / 2.0f;
    }


    [SerializeField] private Rigidbody2D _loopBody = default;
    [SerializeField] private Loop[] _loops = default;


    private void Start()
    {
        foreach (Loop loop in _loops)
            loop.Setup();
    }

    private void FixedUpdate()
    {
        Vector2 totalMovement = Vector2.zero;
        foreach (Loop loop in _loops)
        {
            loop.Update();
            totalMovement += loop.MovementVector;
        }

        _loopBody.MovePosition(_loopBody.position + totalMovement);
    }
}
