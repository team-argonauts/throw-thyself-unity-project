﻿using System.Collections.Generic;
using UnityEngine;
using UnityAtoms.BaseAtoms;


[SelectionBase]
public class Grabbable : MonoBehaviour
{
    [SerializeField] private FloatReference _velocityThreshold = default;

    [SerializeField] private GrabUltEvent OnGrabbed = default;
    [SerializeField] private DetachUltEvent OnDetached = default;

    private readonly List<Grabber> _grabbers = new List<Grabber>();

    public float VelocityThreshold => _velocityThreshold;
    public Rigidbody2D GrabBody { get; private set; } = default;


    private void Awake() => GrabBody = GetComponent<Rigidbody2D>();

    public void Grabbed(Grabber grabber, Vector2 grabPosition, Vector2 grabVelocity, Vector2 grabNormal)
    {
        _grabbers.Add(grabber);
        OnGrabbed?.Invoke(grabPosition, grabVelocity, grabNormal);
    }
    public void Detached(Grabber grabber, Vector2 detachPosition, Vector2 detachNormal)
    {
        _grabbers.Remove(grabber);
        OnDetached?.Invoke(detachPosition, detachNormal);
    }

    public void DetachAll()
    {
        foreach (Grabber grabber in _grabbers.ToArray())
            grabber.Detach();
    }
}
