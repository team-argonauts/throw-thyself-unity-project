﻿using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.Assertions;


/// <summary> Vector2: clashPosition </summary>
[System.Serializable] public class ClashableUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public class Clashable : MonoBehaviour
{
    [SerializeField] private Transform _clashTransform = default;
    [SerializeField] private FloatReference _clashForce = default;

    [SerializeField] private ClashableUltEvent OnClash = default;


    private void DoClashableRepulsion(Clasher otherClasher, Collider2D otherCollider)
    {
        Assert.IsNotNull(_clashTransform);

        Vector2 clashPosition = otherCollider.ClosestPoint(_clashTransform.position);
        Vector2 forceDirection = _clashTransform.position - otherCollider.bounds.center;
        Vector2 clashForce = _clashForce * forceDirection;

        otherClasher.Clash(clashPosition, -clashForce);
        OnClash?.Invoke(clashPosition, clashForce);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Rigidbody2D rb = other.gameObject.GetComponentRelative<Rigidbody2D>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT, false);
        Clasher clasher;
        if (rb) clasher = rb.gameObject.GetComponentRelative<Clasher>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, false);
        else    clasher = other.gameObject.GetComponentRelative<Clasher>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN | eComponentRelativeType.PARENT, false);

        if (clasher == null || !isActiveAndEnabled)
            return;

        DoClashableRepulsion(clasher, other);
    }
}
