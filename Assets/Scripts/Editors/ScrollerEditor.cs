﻿using UnityEditor;
using UnityAtoms;
using UnityEngine;


#if UNITY_EDITOR

[CustomEditor(typeof(Scroller))]
public class ScrollerEditor : Editor
{
    private SerializedProperty _isPhysicalMovementProperty = default;
    private SerializedProperty _scrollTransformProperty = default;
    private SerializedProperty _scrollBodyProperty = default;
    private SerializedProperty _scrollTimeProperty = default;
    private SerializedProperty _axisProperty = default;
    private SerializedProperty _onScrollResetProperty = default;


    private void OnEnable()
    {
        _isPhysicalMovementProperty = serializedObject.FindProperty("_isPhysicalMovement");
        _scrollTransformProperty = serializedObject.FindProperty("_scrollTransform");
        _scrollBodyProperty = serializedObject.FindProperty("_scrollBody");
        _scrollTimeProperty = serializedObject.FindProperty("_scrollTime");
        _axisProperty = serializedObject.FindProperty("_axis");
        _onScrollResetProperty = serializedObject.FindProperty("OnScrollReset");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(_isPhysicalMovementProperty);

        SerializedProperty valueProperty = _isPhysicalMovementProperty.GetValueFromAtomReference();
        bool isPhysicalProperty = valueProperty == null ? default : valueProperty.boolValue;
        EditorGUILayout.PropertyField(GetMovementBodyProperty(isPhysicalProperty));

        EditorGUILayout.PropertyField(_scrollTimeProperty);
        EditorGUILayout.PropertyField(_axisProperty);
        EditorGUILayout.PropertyField(_onScrollResetProperty);

        serializedObject.ApplyModifiedProperties();
    }

    private SerializedProperty GetMovementBodyProperty(bool isPhysicalMovement) => isPhysicalMovement ? _scrollBodyProperty : _scrollTransformProperty;
}

#endif
