﻿using UnityEngine;
using UltEvents;


public class GodPowerControlReciever : MonoBehaviour
{
    [SerializeField] private UltEvent OnGodPowerEvent = default;

    private void OnGodPower() => OnGodPowerEvent?.Invoke();
}
