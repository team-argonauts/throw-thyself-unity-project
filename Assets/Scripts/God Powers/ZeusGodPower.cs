﻿using System.Collections;
using UnityAtoms.BaseAtoms;
using UnityEngine;


/// <summary> Vector2: powerPosition </summary>
[System.Serializable] public class ZeusBeforePowerUltEvent : UltEvents.UltEvent<Vector2> { }
/// <summary> Vector2: powerPosition, Vector2: powerForce </summary>
[System.Serializable] public class ZeusAfterPowerUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public class ZeusGodPower : MonoBehaviour, IGodPower
{
    [SerializeField] private Rigidbody2D _powerBody = default;
    [SerializeField] private GameObject _teleportParent = default;

    [SerializeField] private Vector2Reference _aimDirectionInput = default;
    [SerializeField] private FloatReference _teleportDistance = default;
    [SerializeField] private FloatReference _powerStrength = default;

    [SerializeField] private ZeusBeforePowerUltEvent OnBeforeZeusPower = default;
    [SerializeField] private ZeusAfterPowerUltEvent OnAfterZeusPower = default;


    public void ActivateGodPower() => StartCoroutine(GodPowerRoutine());

    private IEnumerator GodPowerRoutine()
    {
        OnBeforeZeusPower?.Invoke(_powerBody.position);

        Vector2 powerVelocity = _aimDirectionInput.Value * _powerStrength.Value;

        _powerBody.velocity = Vector2.zero;
        _powerBody.AddVelocityChangeForce(powerVelocity);
        _powerBody.SwingInstantTowardsDirection(_aimDirectionInput.Value);

        Vector2 teleportVector = _aimDirectionInput.Value * _teleportDistance.Value;
        Teleport(teleportVector);

        // Delay to allow teleport to actually move the transforms.
        yield return new WaitForSeconds(0.05f);

        OnAfterZeusPower?.Invoke(_powerBody.position, powerVelocity);
    }

    private void Teleport(Vector2 teleportVector)
    {
        foreach (Rigidbody2D teleportBody in _teleportParent.GetComponentsRelative<Rigidbody2D>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, true))
            teleportBody.MovePosition(teleportBody.position + teleportVector);
    }
}
