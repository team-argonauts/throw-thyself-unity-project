﻿using System.Collections;
using UltEvents;
using UnityAtoms.BaseAtoms;
using UnityEngine;


/// <summary> Vector2: powerForce/Direction </summary>
[System.Serializable] public class RaPowerUltEvent : UltEvent<Vector2> { }

public class RaGodPower : MonoBehaviour, IGodPower
{
    [SerializeField] private Rigidbody2D _powerBody = default;
    [SerializeField] private FloatReference _powerHoldTime = default;
    [SerializeField] private FloatReference _powerEndForce = default;
    [SerializeField] private Vector2Reference _aimDirectionInput = default;

    [SerializeField] private UltEvent OnRaPowerStart = default;
    [SerializeField] private RaPowerUltEvent OnRaPowerStay = default;
    [SerializeField] private RaPowerUltEvent OnRaPowerEnd = default;


    private bool _canActivatePower = true;


    public void ActivateGodPower()
    {
        if (_canActivatePower)
            StartCoroutine(PowerRoutine());
    }

    private IEnumerator PowerRoutine()
    {
        StartPower();

        TimeTracker powerTimer = new TimeTracker();
        powerTimer.Set(_powerHoldTime.Value);

        while (!powerTimer.Stopped)
        {
            StayPower();
            yield return null;
        }

        EndPower();
    }

    private void StartPower()
    {
        _powerBody.velocity = Vector2.zero;
        _powerBody.angularVelocity = 0.0f;

        _canActivatePower = false;
        OnRaPowerStart?.Invoke();
    }

    private void StayPower()
    {
        _powerBody.velocity = Vector2.zero;
        OnRaPowerStay?.Invoke(_aimDirectionInput.Value);
    }

    private void EndPower()
    {
        Vector2 endPowerVelocity = _aimDirectionInput.Value * _powerEndForce.Value;
        _powerBody.ChangeVelocity(endPowerVelocity);
        _powerBody.SwingInstantTowardsDirection(_aimDirectionInput.Value);

        _canActivatePower = true;
        OnRaPowerEnd?.Invoke(endPowerVelocity);
    }
}
