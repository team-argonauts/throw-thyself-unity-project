﻿using UnityAtoms.BaseAtoms;
using UnityEngine;


/// <summary> Vector2: powerPosition, Vector2: powerForce </summary>
[System.Serializable] public class ThorPowerUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public class ThorGodPower : MonoBehaviour, IGodPower
{
    [SerializeField] private Rigidbody2D _powerBody = default;
    [SerializeField] private FloatReference _powerStrength = default;
    [SerializeField] private ThorPowerUltEvent OnThorPower = default;

    public void ActivateGodPower()
    {
        Vector2 powerVelocity = _powerStrength.Value * Vector2.down;
        _powerBody.ChangeVelocity(powerVelocity);
        _powerBody.SwingInstantTowardsDirection(Vector2.down);

        OnThorPower?.Invoke(_powerBody.position, powerVelocity);
    }
}
