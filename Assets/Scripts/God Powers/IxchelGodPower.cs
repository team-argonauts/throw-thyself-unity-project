﻿using UnityAtoms.BaseAtoms;
using UnityEngine;


/// <summary> Vector2: powerPosition, Vector2: powerForce </summary>
[System.Serializable] public class IxchelPowerUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public class IxchelGodPower : MonoBehaviour, IGodPower
{
    [SerializeField] private Rigidbody2D _powerBody = default;
    [SerializeField] private FloatReference _powerStrength = default;
    [SerializeField] private IxchelPowerUltEvent OnIxchelPower = default;

    public void ActivateGodPower()
    {
        Vector2 powerVelocity = _powerStrength.Value * Vector2.up;
        _powerBody.ChangeVelocity(powerVelocity);
        _powerBody.SwingInstantTowardsDirection(Vector2.up);

        OnIxchelPower?.Invoke(_powerBody.position, powerVelocity);
    }
}
