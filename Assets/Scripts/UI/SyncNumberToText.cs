﻿using TMPro;
using UnityAtoms.BaseAtoms;
using UnityEngine;


public class SyncNumberToText : MonoBehaviour
{
    [SerializeField] private TMP_Text _text = default;
    [SerializeField] private BoolReference _useFloat = default;


    public void SyncNumber(float number)
    {
        if (!_useFloat)
            _text.text = ((int)number).ToString();
        else 
            _text.text = number.ToString();
    }

    public void SyncNumber(int number)
    {
        if (_useFloat)
            _text.text = ((float)number).ToString();
        else
            _text.text = number.ToString();
    }
}
