﻿using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;


public static class GamepadHelper
{
    public static Gamepad GetGamepadFromGameObject(GameObject gameObject)
    {
        PlayerInput playerInput = GetPlayerInputFromGameObject(gameObject);
        Gamepad foundGamepad = GetGamepadFromPlayerInput(playerInput);

        return foundGamepad;
    }

    public static PlayerInput GetPlayerInputFromGameObject(GameObject gameObject)
    {
        PlayerInput playerInput;

        if ((playerInput = gameObject.GetComponent<PlayerInput>()) != null)
            return playerInput;
        if ((playerInput = gameObject.GetComponentInParent<PlayerInput>()) != null)
            return playerInput;
        if ((playerInput = gameObject.GetComponentInChildren<PlayerInput>()) != null)
            return playerInput;

        return null;
    }

    public static Gamepad GetGamepadFromPlayerInput(PlayerInput playerInput)
    {
        if (playerInput == null)
            return null;

        return Gamepad.all.FirstOrDefault(g => playerInput.devices.Any(d => d.deviceId == g.deviceId));
    }
}