﻿using System.Linq;
using UnityAtoms;


public static class PlayerHelper
{
    public static bool PlayerAlive(GodValueList godList, Player player) => godList.Any((p) => p.GodObject == player.InstantiatedGod);
    public static Player GetPlayerFromGod(God god, PlayerValueList playerList) => playerList.FirstOrDefault((p) => p.InstantiatedGod == god.GodObject);
}