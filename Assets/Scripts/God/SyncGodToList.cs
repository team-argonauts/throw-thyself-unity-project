﻿using UnityEngine;
using UnityEngine.Assertions;

namespace UnityAtoms.BaseAtoms
{
    /// <summary>
    /// Adds a God to a God Value List on OnEnable and removes it on OnDisable.
    /// </summary>
    [AddComponentMenu("Unity Atoms/MonoBehaviour Helpers/Sync God To List")]
    [EditorIcon("atom-icon-delicate")]
    public class SyncGodToList : MonoBehaviour
    {
        [SerializeField] private GodReference _god = default;
        [SerializeField] private GodValueList _godList = default;
        
        public God God => _god.Value;


        void OnEnable()
        {
            Assert.IsNotNull(_godList);
            _godList.Add(_god);
        }

        private void OnDisable()
        {
            Assert.IsNotNull(_godList);
            _godList.Remove(_god);
        }
    }
}
