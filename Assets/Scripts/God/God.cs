﻿using System;
using UnityEngine;


[Serializable]
public struct God
{
    [SerializeField] private GameObject _godObject;
    [SerializeField] private Transform _followTarget;
    [SerializeField] private Rigidbody2D _weaponBody;
    [SerializeField] private RagdollExploder _ragdollExploder;

    public GameObject GodObject => _godObject;
    public Transform FollowTarget => _followTarget;
    public Rigidbody2D WeaponBody => _weaponBody;
    public RagdollExploder RagdollExploder => _ragdollExploder;


    public God(GameObject godObject, Transform followTarget, Rigidbody2D weaponBody, RagdollExploder ragdollExploder)
    {
        _godObject = godObject;
        _followTarget = followTarget;
        _weaponBody = weaponBody;
        _ragdollExploder = ragdollExploder;
    }
}
