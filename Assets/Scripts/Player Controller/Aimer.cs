﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityAtoms.BaseAtoms;

public class Aimer : MonoBehaviour
{
    [SerializeField] private Transform _aimBody = null;
    [SerializeField] private Vector2Reference _currentAimDirection = null;
    [SerializeField] private Vector2Reference _lastNonZeroAimDirection = null;

    private Vector2 _internalAimDirection = Vector2.zero;
    private Camera _mainCamera = null;


    private void OnEnable() => _mainCamera = Camera.main;
    private void OnDisable() => _mainCamera = null;

    private static bool AimIsValid(Vector2 aimDirection) => aimDirection.sqrMagnitude != 0;

    private void Update()
    {
        _currentAimDirection.Value = _internalAimDirection;

        if (AimIsValid(_internalAimDirection))
            _lastNonZeroAimDirection.Value = _internalAimDirection;
    }

    public void AimGamepad(Vector2 aimDirection) => _internalAimDirection = aimDirection.normalized;

    public void AimMouse(Vector2 mousePosition)
    {
        if (_mainCamera == null)
            return;
        
        Assert.IsNotNull(_aimBody, "Aim body not assigned");

        Vector2 controlBodyScreenPoint = _mainCamera.WorldToScreenPoint(_aimBody.position);
        Vector2 newAimDirection = (mousePosition - controlBodyScreenPoint).normalized;

        _internalAimDirection = newAimDirection;
    }
}