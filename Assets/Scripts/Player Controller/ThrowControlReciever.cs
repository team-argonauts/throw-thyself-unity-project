﻿using UnityEngine;
using UltEvents;


public class ThrowControlReciever : MonoBehaviour
{
    [SerializeField] private UltEvent OnThrowDownEvent = default;
    [SerializeField] private UltEvent OnThrowUpEvent = default;
    

    private void OnThrowDown() => OnThrowDownEvent?.Invoke();
    private void OnThrowUp() => OnThrowUpEvent?.Invoke();
}
