﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityAtoms.BaseAtoms;


public class AimControlReciever : MonoBehaviour
{
    [SerializeField] private Vector2UltEvent OnAimGamepadEvent = default;
    [SerializeField] private Vector2UltEvent OnAimMouseEvent = default;
    

    private void OnAimGamepad(InputValue value) => OnAimGamepadEvent?.Invoke(value.Get<Vector2>());
    private void OnAimMouse(InputValue value) => OnAimMouseEvent?.Invoke(value.Get<Vector2>());
}
