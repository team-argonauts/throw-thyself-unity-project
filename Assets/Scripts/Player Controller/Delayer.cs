﻿using UnityEngine;
using UltEvents;
using UnityAtoms.BaseAtoms;
using System.Collections;


[System.Serializable] public class DelayUltEvent : UltEvent<float, float> { }


public class Delayer : MonoBehaviour
{
    [SerializeField] private FloatReference _duration = default;
    [SerializeField] private BoolReference _countDown = default;

    [SerializeField] private FloatUltEvent OnDelayStart = default;
    [SerializeField] private DelayUltEvent OnDelayUpdate = default;
    [SerializeField] private UltEvent OnDelayEnd = default;
    [SerializeField] private UltEvent OnDelayCancel = default;


    private readonly TimeTracker _delayTimer = new TimeTracker();
    private bool _isDelaying = false;


    public void StartDelay()
    {
        _isDelaying = true;
        _delayTimer.Set(_duration.Value);
        OnDelayStart?.Invoke(_duration.Value);
    }

    private void Update()
    {
        if (!_isDelaying)
            return;

        float convertedProgress = _countDown.Value ? 1.0f - _delayTimer.Progress : _delayTimer.Progress;
        OnDelayUpdate?.Invoke(convertedProgress, convertedProgress * _duration.Value);

        if (_delayTimer.Stopped)
            EndDelay();
    }

    private void EndDelay()
    {
        _isDelaying = false;
        OnDelayEnd?.Invoke();
    }

    public void CancelDelay()
    {
        _isDelaying = false;
        OnDelayCancel?.Invoke();
    }
}
