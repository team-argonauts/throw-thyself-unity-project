﻿using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.Assertions;

public class Thrower : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _throwBody = default;
    [SerializeField] private FloatReference _throwStrength = default;

    [SerializeField] private Vector2UltEvent OnThrow = default;


    private Vector2 GetThrowForce(Vector2 aimDirection, float charge) => aimDirection * _throwStrength.Value * charge;

    public void Throw(Vector2 aimDirection, float charge)
    {
        if (charge <= 0.0f)
            return;

        Assert.IsNotNull(_throwBody, "Throw Body Unassigned");


        Vector2 throwVelocity = GetThrowForce(aimDirection, charge);
        _throwBody.AddVelocityChangeForce(throwVelocity);
        _throwBody.SwingInstantTowardsDirection(aimDirection);

        OnThrow?.Invoke(throwVelocity);
    }
}
