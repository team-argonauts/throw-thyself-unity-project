﻿using UnityEngine;
using UnityEngine.Assertions;

[DisallowMultipleComponent]
public class CollisionIgnorer : MonoBehaviour
{
    [SerializeField] private Collider2D[] _otherColliders = default;
    private Collider2D[] MyColliders => gameObject.GetComponentsRelative<Collider2D>(eComponentRelativeType.SELF | eComponentRelativeType.CHILDREN, false);

    private void Start()
    {
        foreach (Collider2D myCollider in MyColliders)
            foreach (Collider2D otherCollider in _otherColliders)
            {
                Assert.IsNotNull(myCollider, "Ignore My Collider Unassigned");
                Assert.IsNotNull(otherCollider, "Ignore Other Collider Unassigned");
                Physics2D.IgnoreCollision(myCollider, otherCollider, true);
            }    
    }
}
