﻿using UnityEngine;


public class Bounceable : MonoBehaviour
{
    [SerializeField] private BounceHitUltEvent OnBounceHit = default;
    [SerializeField] private BounceOffUltEvent OnBounceOff = default;

    public void BounceHit(Vector2 bounceHitPosition, Vector2 bounceHitVelocity, Vector2 bounceHitNormal) => OnBounceHit?.Invoke(bounceHitPosition, bounceHitVelocity, bounceHitNormal);
    public void BounceOff(Vector2 bounceOffPosition, Vector2 bounceOffForce) => OnBounceOff?.Invoke(bounceOffPosition, bounceOffForce);
}