﻿using UnityEngine;


public class Killable : MonoBehaviour
{
    [SerializeField] private KillUltEvent OnKilled = default;

    private bool _dead = false;

    public void Kill(Vector2 killPosition, Vector2 killForce)
    {
        if (_dead)
            return;

        OnKilled?.Invoke(killPosition, killForce);
        _dead = true;
    }
}
