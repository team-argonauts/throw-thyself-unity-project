﻿using UnityEngine;
using UltEvents;
using UnityAtoms.BaseAtoms;


public class Charger : MonoBehaviour
{
    [SerializeField] private FloatReference _chargeProgressOutput = default;
    [SerializeField] private FloatReference _chargeMaxTime = default;
    [SerializeField] private FloatReference _chargeRampup = default;

    [SerializeField] private FloatUltEvent OnChargeStart = default;
    [SerializeField] private FloatUltEvent OnChargeUpdate = default;
    [SerializeField] private UltEvent OnChargeFull = default;
    [SerializeField] private UltEvent OnChargeEnd = default;

    private readonly TimeTracker _chargeTimer = new TimeTracker();
    private bool _preCharging = default;
    private bool _charging = default;
    private bool _previousChargeFull = default;

    public bool CanCharge { get; set; } = true;


    public void StartCharge()
    {
        if (!CanCharge)
        {
            _preCharging = true;
            return;
        }

        _charging = true;
        _chargeTimer.Set(_chargeMaxTime.Value);
        OnChargeStart?.Invoke(_chargeMaxTime);
    }

    private void Update()
    {
        if (!CanCharge)
        {
            if (_charging)
                EndCharge();

            return;
        }

        else if (!_charging)
        {
            if (_preCharging)
            {
                StartCharge();
                _preCharging = false;
            }
            else
                return;
        }

        _chargeProgressOutput.Value = Mathf.Pow(_chargeTimer.Progress, _chargeRampup.Value);
        OnChargeUpdate?.Invoke(_chargeProgressOutput.Value);

        if (_chargeTimer.Stopped && !_previousChargeFull)
            OnChargeFull?.Invoke();

        _previousChargeFull = _chargeTimer.Stopped;
    }

    public void EndCharge()
    {
        _preCharging = false;
        
        if (!_charging)
            return;

        OnChargeEnd?.Invoke();
        _charging = false;
        _chargeProgressOutput.Value = 0.0f;
    }
}
