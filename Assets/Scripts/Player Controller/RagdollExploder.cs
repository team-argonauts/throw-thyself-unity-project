﻿using UnityEngine;
using UltEvents;


public class RagdollExploder : MonoBehaviour
{
    [SerializeField] private Joint2D[] _joints = default;
    [SerializeField] private Rigidbody2D[] _ragdollBodies = default;

    [SerializeField] private UltEvent OnExplode = default;


    public void ExplodeInDirection(float explosionForce, Vector2 explosionDirection) => ExplodeInDirection(explosionForce * explosionDirection.normalized);

    public void ExplodeInDirection(Vector2 explosionForce)
    {
        BreakJoints();
        AddDirectionalExplosionForce(explosionForce);
        OnExplode?.Invoke();
    }

    public void ExplodeAtPoint(float explosionForce, Vector2 explosionPoint, float explosionRadius, float upwardsModifier)
    {
        BreakJoints();
        AddPointExplosionForce(explosionForce, explosionPoint, explosionRadius, upwardsModifier);
        OnExplode?.Invoke();
    }


    public void BreakJoints()
    {
        foreach (Joint2D joint in _joints)
            if (joint != null)
                joint.breakForce = 0.0f;
    }

    public void AddDirectionalExplosionForce(Vector2 explosionForce)
    {
        foreach (Rigidbody2D ragdollBody in _ragdollBodies)
            if (ragdollBody != null)
                ragdollBody.AddVelocityChangeForce(explosionForce);
    }

    public void AddPointExplosionForce(float explosionForce, Vector2 explosionPoint, float explosionRadius, float upwardsModifier)
    {
        foreach (Rigidbody2D ragdollBody in _ragdollBodies)
            if (ragdollBody != null)
                ragdollBody.AddVelocityChangeExplosionForce(explosionForce, explosionPoint, explosionRadius, upwardsModifier);
    }
}
