﻿using UnityEngine;


/// <summary> Vector2: clashPosition </summary>
[System.Serializable] public class ClasherUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public class Clasher : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _clashBody = default;

    [SerializeField] private ClasherUltEvent OnClash = default;

    private Vector2 _delayedClashVelocity = Vector2.zero;


    public void Clash(Vector2 clashPosition, Vector2 clashForce)
    {
        _delayedClashVelocity += clashForce;
        OnClash?.Invoke(clashPosition, clashForce);
    }

    private void Update()
    {
        if (_delayedClashVelocity.sqrMagnitude == 0.0f)
            return;

        _clashBody.ChangeVelocity(_delayedClashVelocity);
        _delayedClashVelocity = Vector2.zero;
    }
}
