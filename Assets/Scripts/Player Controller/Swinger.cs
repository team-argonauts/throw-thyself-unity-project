﻿using UnityEngine;
using UnityAtoms.BaseAtoms;
using UnityEngine.Assertions;


public class Swinger : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _swingBody = default;
    [SerializeField] private Vector2Reference _swingAimDirectionInput = default;
    [SerializeField] private FloatReference _swingAimChangeStrength = default;
    [SerializeField] private FloatReference _swingAimContinuousStrength = default;


    private void FixedUpdate()
    {
        if (_swingAimDirectionInput.Value == Vector2.zero)
            return;

        Assert.IsNotNull(_swingBody);
        _swingBody.AddTorque(GetStrafeSwingForce() * Time.fixedDeltaTime, ForceMode2D.Impulse);
    }

    private float GetStrafeSwingForce()
    {
        float swingAngle = Mathf.DeltaAngle(_swingBody.rotation, _swingAimDirectionInput.Value.Angle());
        float scaledSwingAngle = swingAngle / 180.0f;

        float continuousTorqueForce = scaledSwingAngle * _swingAimChangeStrength.Value;
        float changeSwingTorque = Mathf.Sign(scaledSwingAngle) * _swingAimContinuousStrength.Value;

        return continuousTorqueForce + changeSwingTorque;
    }
}
