﻿using System.Collections.Generic;
using UnityEngine;


/// <summary> Vector2: grabPosition, Vector2: grabVelocity, Vector2: grabNormal </summary>
[System.Serializable] public class GrabUltEvent : UltEvents.UltEvent<Vector2, Vector2, Vector2> { }

/// <summary> Vector2: detachPosition, Vector2: detachNormal </summary>
[System.Serializable] public class DetachUltEvent : UltEvents.UltEvent<Vector2, Vector2> { }


public class Grabber : MonoBehaviour
{
    private struct GrabPoint
    {
        public Vector2 Position { get; private set; }
        public Vector2 Normal { get; private set; }
        public Grabbable Grabbable { get; private set; }
        public FixedJoint2D Joint { get; private set; }

        public GrabPoint(Vector2 position, Vector2 normal, Grabbable grabbable, FixedJoint2D joint)
        {
            Position = position;
            Normal = normal;
            Grabbable = grabbable;
            Joint = joint;
        }
    }


    [SerializeField] private Rigidbody2D _controlBody = default;
    private readonly List<GrabPoint> _grabPoints = new List<GrabPoint>();

    public bool IsGrabbing { get => _grabPoints.Count > 0; }

    [SerializeField] private GrabUltEvent OnGrab = default;
    [SerializeField] private DetachUltEvent OnDetach = default;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        Grabbable grabbable = collision.gameObject.GetComponentRelative<Grabbable>(eComponentRelativeType.SELF | eComponentRelativeType.PARENT, false);
        if (grabbable == null || !isActiveAndEnabled)
            return;

        Vector2 collisionNormal = collision.GetAverageCollisionNormal();
        Vector2 normalVelocity = collisionNormal * collision.relativeVelocity;
        Vector2 localVelocity = _controlBody.transform.InverseTransformDirection(normalVelocity);
        if (Mathf.Abs(localVelocity.magnitude) <= grabbable.VelocityThreshold)
            return;

        Vector2 collisionPoint = collision.GetAverageCollisionPoint();
        Grab(grabbable, collisionPoint, collision.relativeVelocity, collisionNormal);
    }

    private void Grab(Grabbable grabbable, Vector2 grabPosition, Vector2 grabVelocity, Vector2 grabNormal)
    {
        FixedJoint2D newGrabJoint = _controlBody.gameObject.AddComponent<FixedJoint2D>();
        newGrabJoint.connectedBody = grabbable.GrabBody;
        newGrabJoint.enableCollision = true;

        _grabPoints.Add(new GrabPoint(grabPosition, grabNormal, grabbable, newGrabJoint));

        OnGrab?.Invoke(grabPosition, grabVelocity, grabNormal);
        grabbable.Grabbed(this, grabPosition, grabVelocity, grabNormal);
    }


    public void Detach()
    {
        if (!IsGrabbing)
            return;


        foreach (GrabPoint grabPoint in _grabPoints)
        {
            grabPoint.Joint.connectedBody = null;
            Destroy(grabPoint.Joint);

            OnDetach.Invoke(grabPoint.Position, grabPoint.Normal);
            grabPoint.Grabbable.Detached(this, grabPoint.Position, grabPoint.Normal);
        }

        _grabPoints.Clear();
    }

    private void OnDisable() => Detach();
}