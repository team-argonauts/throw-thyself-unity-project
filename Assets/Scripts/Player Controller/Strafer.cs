﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityAtoms.BaseAtoms;

public class Strafer : MonoBehaviour
{
    [SerializeField] private Vector2Reference _strafeDirectionInput = default;
    [SerializeField] private Rigidbody2D _strafeBody = default;
    [SerializeField] private FloatReference _strafeChangeStrength = default;
    [SerializeField] private FloatReference _strafeContinuousStrength = default;

    private Vector2 _prevAimDirection = default;


    private Vector2 GetStrafeForce()
    {
        Vector2 aimDelta = _strafeDirectionInput.Value - _prevAimDirection;
        Vector2 strafeChangeForce = aimDelta * _strafeChangeStrength.Value;
        Vector2 strafeContinuousForce = _strafeDirectionInput.Value * _strafeContinuousStrength.Value;

        return strafeChangeForce + strafeContinuousForce;
    }

    private void FixedUpdate()
    {
        Assert.IsNotNull(_strafeBody, "Strafe Body Unassigned");

        _strafeBody.AddVelocityChangeForce(GetStrafeForce() * Time.fixedDeltaTime);
        _prevAimDirection = _strafeDirectionInput.Value;
    }
}