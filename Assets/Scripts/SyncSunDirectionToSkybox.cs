﻿using UnityEngine;

[ExecuteAlways]
public class SyncSunDirectionToSkybox : MonoBehaviour
{
    private void Update() => Shader.SetGlobalVector("_SunDirection", transform.forward);
}
