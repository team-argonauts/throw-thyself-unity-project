﻿using UnityEngine;


public static class Collision2DExtensions
{
    public static Vector2 GetAverageCollisionPoint(this Collision2D collision)
    {
        Vector2 averageCollisionPoint = Vector2.zero;

        for (int i = 0; i < collision.contactCount; ++i)
            averageCollisionPoint += collision.GetContact(i).point;

        return averageCollisionPoint / collision.contactCount;
    }

    public static Vector2 GetAverageCollisionNormal(this Collision2D collision)
    {
        Vector2 averageCollisionNormal = Vector2.zero;

        for (int i = 0; i < collision.contactCount; ++i)
            averageCollisionNormal += collision.GetContact(i).normal;

        return (averageCollisionNormal / collision.contactCount).normalized;
    }
}