﻿using UnityEngine;


public static class Rigidbody2DExtensions
{
    public static void AddVelocityChangeExplosionForce(this Rigidbody2D rigidbody, float explosionForce, Vector2 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F)
    {
        Vector2 bodyDelta = rigidbody.position - explosionPosition;
        float explosionDistance = bodyDelta.magnitude;

        Vector2 explosionDirection = (bodyDelta + (Vector2.up * upwardsModifier)).normalized;

        float relativeExplosionForce = Mathf.Lerp(0, explosionForce, explosionRadius - explosionDistance);
        Vector2 amplifiedExplosionForce = relativeExplosionForce * explosionDirection;

        rigidbody.AddForce(amplifiedExplosionForce * rigidbody.mass, ForceMode2D.Impulse);
    }

    public static void ChangeVelocity(this Rigidbody2D rigidbody, Vector2 newVelocity) => rigidbody.velocity = newVelocity;

    public static void AddVelocityChangeForce(this Rigidbody2D rigidbody, Vector2 velocityChangeForce) => rigidbody.AddForce(velocityChangeForce * rigidbody.mass, ForceMode2D.Impulse);

    public static void SwingInstantTowardsDirection(this Rigidbody2D rigidbody, Vector2 swingDirection)
    {
        rigidbody.angularVelocity = 0.0f;
        rigidbody.rotation = swingDirection.normalized.Angle();
    }
}