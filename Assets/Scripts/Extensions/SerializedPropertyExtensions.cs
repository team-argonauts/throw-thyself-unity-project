﻿using UnityEditor;
using UnityAtoms;


#if UNITY_EDITOR

public static class SerializedPropertyExtensions
{
    public static SerializedProperty GetValueFromAtomReference(this SerializedProperty atomReference)
    {
        int usage = atomReference.FindPropertyRelative("_usage").intValue;
        SerializedProperty valueProperty;
        switch (usage)
        {
            case (AtomReferenceUsage.CONSTANT):
                valueProperty = atomReference.FindPropertyRelative("_constant");
                break;
            case (AtomReferenceUsage.VARIABLE):
                valueProperty = atomReference.FindPropertyRelative("_variable");
                break;
            case (AtomReferenceUsage.VARIABLE_INSTANCER):
                valueProperty = atomReference.FindPropertyRelative("_variableInstancer");
                break;
            default:
            case (AtomReferenceUsage.VALUE):
                return atomReference.FindPropertyRelative("_value");
        }

        if (valueProperty.objectReferenceValue == null)
            return default;

        return new SerializedObject(valueProperty.objectReferenceValue).FindProperty("_value"); // Hacky solution to get the value due to errors with unity serialisation.
    }
}

#endif