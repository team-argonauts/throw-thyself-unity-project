﻿using UnityEngine;


public static class RendererExtensions
{
    public static void EnableKeyword(this Renderer renderer, string keyword, int materialPosition)
    {
        if (renderer != null)
            renderer.materials[materialPosition].EnableKeyword(keyword);
    }
    public static void DisableKeyword(this Renderer renderer, string keyword, int materialPosition)
    {
        if (renderer != null)
            renderer.materials[materialPosition].DisableKeyword(keyword);
    }
    public static void SetColor(this Renderer renderer, string colorName, Color color, int materialPosition)
    {
        if (renderer != null)
            renderer.materials[materialPosition].SetColor(colorName, color);
    }

    public static void SetColor(this Renderer renderer, string colorName, Color color, float intensity, int materialPosition)
    {
        if (renderer != null)
        {
            float factor = Mathf.Pow(2, intensity);
            renderer.materials[materialPosition].SetColor(colorName, color * factor);
        }
    }
}