using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `Player`. Inherits from `AtomEvent&lt;Player&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/Player", fileName = "PlayerEvent")]
    public sealed class PlayerEvent : AtomEvent<Player>
    {
    }
}
