using UnityEngine;
using System;

namespace UnityAtoms
{
    /// <summary>
    /// Variable of type `Player`. Inherits from `AtomVariable&lt;Player, PlayerPair, PlayerEvent, PlayerPairEvent, PlayerPlayerFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-lush")]
    [CreateAssetMenu(menuName = "Unity Atoms/Variables/Player", fileName = "PlayerVariable")]
    public sealed class PlayerVariable : AtomVariable<Player, PlayerPair, PlayerEvent, PlayerPairEvent, PlayerPlayerFunction>
    {
        protected override bool ValueEquals(Player other) => Value == other;
    }
}
