using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Unity Event of type `PlayerPair`. Inherits from `UltEvent&lt;PlayerPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerPairUltEvent : UltEvent<PlayerPair> { }
}
