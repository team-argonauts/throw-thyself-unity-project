using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Unity Event of type `Player`. Inherits from `UltEvent&lt;Player&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerUltEvent : UltEvent<Player> { }
}
