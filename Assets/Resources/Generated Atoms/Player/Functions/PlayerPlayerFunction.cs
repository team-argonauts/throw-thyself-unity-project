namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `Player`. Inherits from `AtomFunction&lt;Player, Player&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class PlayerPlayerFunction : AtomFunction<Player, Player> { }
}
