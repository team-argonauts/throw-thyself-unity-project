namespace UnityAtoms
{
    /// <summary>
    /// Action of type `PlayerPair`. Inherits from `AtomAction&lt;PlayerPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class PlayerPairAction : AtomAction<PlayerPair> { }
}
