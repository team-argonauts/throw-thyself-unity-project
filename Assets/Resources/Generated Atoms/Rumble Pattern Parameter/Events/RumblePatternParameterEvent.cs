using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `RumblePatternParameter`. Inherits from `AtomEvent&lt;RumblePatternParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/RumblePatternParameter", fileName = "RumblePatternParameterEvent")]
    public sealed class RumblePatternParameterEvent : AtomEvent<RumblePatternParameter>
    {
    }
}
