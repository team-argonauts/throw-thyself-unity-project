using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `RumblePatternParameterPair`. Inherits from `AtomEvent&lt;RumblePatternParameterPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/RumblePatternParameterPair", fileName = "RumblePatternParameterPairEvent")]
    public sealed class RumblePatternParameterPairEvent : AtomEvent<RumblePatternParameterPair>
    {
    }
}
