using UnityEngine;
using System;

namespace UnityAtoms
{
    /// <summary>
    /// Variable of type `RumblePatternParameter`. Inherits from `AtomVariable&lt;RumblePatternParameter, RumblePatternParameterPair, RumblePatternParameterEvent, RumblePatternParameterPairEvent, RumblePatternParameterRumblePatternParameterFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-lush")]
    [CreateAssetMenu(menuName = "Unity Atoms/Variables/RumblePatternParameter", fileName = "RumblePatternParameterVariable")]
    public sealed class RumblePatternParameterVariable : AtomVariable<RumblePatternParameter, RumblePatternParameterPair, RumblePatternParameterEvent, RumblePatternParameterPairEvent, RumblePatternParameterRumblePatternParameterFunction>
    {
        protected override bool ValueEquals(RumblePatternParameter other)
        {
            if (Value.LowA != other.LowA)
                return false;
            if (Value.HighA != other.HighA)
                return false;
            if (Value.Pattern != other.Pattern)
                return false;
            if (Value.Duration != other.Duration)
                return false;

            return true;
        }
    }
}
