using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Value List of type `RumblePatternParameter`. Inherits from `AtomValueList&lt;RumblePatternParameter, RumblePatternParameterEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-piglet")]
    [CreateAssetMenu(menuName = "Unity Atoms/Value Lists/RumblePatternParameter", fileName = "RumblePatternParameterValueList")]
    public sealed class RumblePatternParameterValueList : AtomValueList<RumblePatternParameter, RumblePatternParameterEvent> { }
}
