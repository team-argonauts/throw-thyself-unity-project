using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `RumblePatternParameterPair`. Inherits from `AtomEventReference&lt;RumblePatternParameterPair, RumblePatternParameterVariable, RumblePatternParameterPairEvent, RumblePatternParameterVariableInstancer, RumblePatternParameterPairEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumblePatternParameterPairEventReference : AtomEventReference<
        RumblePatternParameterPair,
        RumblePatternParameterVariable,
        RumblePatternParameterPairEvent,
        RumblePatternParameterVariableInstancer,
        RumblePatternParameterPairEventInstancer>, IGetEvent 
    { }
}
