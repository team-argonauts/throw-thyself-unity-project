using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `RumblePatternParameter`. Inherits from `AtomEventReference&lt;RumblePatternParameter, RumblePatternParameterVariable, RumblePatternParameterEvent, RumblePatternParameterVariableInstancer, RumblePatternParameterEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumblePatternParameterEventReference : AtomEventReference<
        RumblePatternParameter,
        RumblePatternParameterVariable,
        RumblePatternParameterEvent,
        RumblePatternParameterVariableInstancer,
        RumblePatternParameterEventInstancer>, IGetEvent 
    { }
}
