using System;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Reference of type `RumblePatternParameter`. Inherits from `AtomReference&lt;RumblePatternParameter, RumblePatternParameterPair, RumblePatternParameterConstant, RumblePatternParameterVariable, RumblePatternParameterEvent, RumblePatternParameterPairEvent, RumblePatternParameterRumblePatternParameterFunction, RumblePatternParameterVariableInstancer, AtomCollection, AtomList&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumblePatternParameterReference : AtomReference<
        RumblePatternParameter,
        RumblePatternParameterPair,
        RumblePatternParameterConstant,
        RumblePatternParameterVariable,
        RumblePatternParameterEvent,
        RumblePatternParameterPairEvent,
        RumblePatternParameterRumblePatternParameterFunction,
        RumblePatternParameterVariableInstancer>, IEquatable<RumblePatternParameterReference>
    {
        public RumblePatternParameterReference() : base() { }
        public RumblePatternParameterReference(RumblePatternParameter value) : base(value) { }
        public bool Equals(RumblePatternParameterReference other) { return base.Equals(other); }
        protected override bool ValueEquals(RumblePatternParameter other)
        {
            throw new NotImplementedException();
        }
    }
}
