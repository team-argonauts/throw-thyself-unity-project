using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Constant of type `RumblePatternParameter`. Inherits from `AtomBaseVariable&lt;RumblePatternParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-teal")]
    [CreateAssetMenu(menuName = "Unity Atoms/Constants/RumblePatternParameter", fileName = "RumblePatternParameterConstant")]
    public sealed class RumblePatternParameterConstant : AtomBaseVariable<RumblePatternParameter> { }
}
