using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `RumblePatternParameter`. Inherits from `AtomEventReferenceListener&lt;RumblePatternParameter, RumblePatternParameterEvent, RumblePatternParameterEventReference, RumblePatternParameterUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/RumblePatternParameter Event Reference Listener")]
    public sealed class RumblePatternParameterEventReferenceListener : AtomEventReferenceListener<
        RumblePatternParameter,
        RumblePatternParameterEvent,
        RumblePatternParameterEventReference,
        RumblePatternParameterUltEvent>
    { }
}
