using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `RumblePatternParameterPair`. Inherits from `AtomEventReferenceListener&lt;RumblePatternParameterPair, RumblePatternParameterPairEvent, RumblePatternParameterPairEventReference, RumblePatternParameterPairUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/RumblePatternParameterPair Event Reference Listener")]
    public sealed class RumblePatternParameterPairEventReferenceListener : AtomEventReferenceListener<
        RumblePatternParameterPair,
        RumblePatternParameterPairEvent,
        RumblePatternParameterPairEventReference,
        RumblePatternParameterPairUltEvent>
    { }
}
