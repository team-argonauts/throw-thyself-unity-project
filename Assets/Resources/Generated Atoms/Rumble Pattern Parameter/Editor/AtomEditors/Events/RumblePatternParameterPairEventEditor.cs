#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityEngine.UIElements;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `RumblePatternParameterPair`. Inherits from `AtomEventEditor&lt;RumblePatternParameterPair, RumblePatternParameterPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomEditor(typeof(RumblePatternParameterPairEvent))]
    public sealed class RumblePatternParameterPairEventEditor : AtomEventEditor<RumblePatternParameterPair, RumblePatternParameterPairEvent> { }
}
#endif
