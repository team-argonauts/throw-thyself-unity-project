using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable Inspector of type `RumblePatternParameter`. Inherits from `AtomVariableEditor`
    /// </summary>
    [CustomEditor(typeof(RumblePatternParameterVariable))]
    public sealed class RumblePatternParameterVariableEditor : AtomVariableEditor<RumblePatternParameter, RumblePatternParameterPair> { }
}
