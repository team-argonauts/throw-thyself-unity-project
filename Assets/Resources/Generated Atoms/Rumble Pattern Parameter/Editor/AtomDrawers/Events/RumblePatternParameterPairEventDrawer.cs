#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `RumblePatternParameterPair`. Inherits from `AtomDrawer&lt;RumblePatternParameterPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumblePatternParameterPairEvent))]
    public class RumblePatternParameterPairEventDrawer : AtomDrawer<RumblePatternParameterPairEvent> { }
}
#endif
