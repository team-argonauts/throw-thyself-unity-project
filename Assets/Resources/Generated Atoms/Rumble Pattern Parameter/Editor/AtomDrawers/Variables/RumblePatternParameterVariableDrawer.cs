#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable property drawer of type `RumblePatternParameter`. Inherits from `AtomDrawer&lt;RumblePatternParameterVariable&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumblePatternParameterVariable))]
    public class RumblePatternParameterVariableDrawer : VariableDrawer<RumblePatternParameterVariable> { }
}
#endif
