#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Value List property drawer of type `RumblePatternParameter`. Inherits from `AtomDrawer&lt;RumblePatternParameterValueList&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumblePatternParameterValueList))]
    public class RumblePatternParameterValueListDrawer : AtomDrawer<RumblePatternParameterValueList> { }
}
#endif
