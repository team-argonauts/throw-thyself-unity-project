#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Constant property drawer of type `RumblePatternParameter`. Inherits from `AtomDrawer&lt;RumblePatternParameterConstant&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumblePatternParameterConstant))]
    public class RumblePatternParameterConstantDrawer : VariableDrawer<RumblePatternParameterConstant> { }
}
#endif
