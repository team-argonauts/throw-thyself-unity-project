using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `RumblePatternParameterPair`. Inherits from `UltEvent&lt;RumblePatternParameterPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumblePatternParameterPairUltEvent : UltEvent<RumblePatternParameterPair> { }
}
