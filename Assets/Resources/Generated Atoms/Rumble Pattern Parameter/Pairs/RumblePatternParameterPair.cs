using System;
using UnityEngine;
namespace UnityAtoms
{
    /// <summary>
    /// IPair of type `&lt;RumblePatternParameter&gt;`. Inherits from `IPair&lt;RumblePatternParameter&gt;`.
    /// </summary>
    [Serializable]
    public struct RumblePatternParameterPair : IPair<RumblePatternParameter>
    {
        public RumblePatternParameter Item1 { get => _item1; set => _item1 = value; }
        public RumblePatternParameter Item2 { get => _item2; set => _item2 = value; }

        [SerializeField]
        private RumblePatternParameter _item1;
        [SerializeField]
        private RumblePatternParameter _item2;

        public void Deconstruct(out RumblePatternParameter item1, out RumblePatternParameter item2) { item1 = Item1; item2 = Item2; }
    }
}