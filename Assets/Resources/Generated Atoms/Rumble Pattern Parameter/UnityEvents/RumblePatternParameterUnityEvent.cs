using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `RumblePatternParameter`. Inherits from `UltEvent&lt;RumblePatternParameter&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumblePatternParameterUltEvent : UltEvent<RumblePatternParameter> { }
}
