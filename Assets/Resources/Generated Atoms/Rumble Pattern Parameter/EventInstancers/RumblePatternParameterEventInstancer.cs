using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `RumblePatternParameter`. Inherits from `AtomEventInstancer&lt;RumblePatternParameter, RumblePatternParameterEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/RumblePatternParameter Event Instancer")]
    public class RumblePatternParameterEventInstancer : AtomEventInstancer<RumblePatternParameter, RumblePatternParameterEvent> { }
}
