using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `RumblePatternParameterPair`. Inherits from `AtomEventInstancer&lt;RumblePatternParameterPair, RumblePatternParameterPairEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/RumblePatternParameterPair Event Instancer")]
    public class RumblePatternParameterPairEventInstancer : AtomEventInstancer<RumblePatternParameterPair, RumblePatternParameterPairEvent> { }
}
