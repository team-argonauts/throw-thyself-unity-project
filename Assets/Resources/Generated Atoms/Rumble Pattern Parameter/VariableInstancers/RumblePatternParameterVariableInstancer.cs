using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Variable Instancer of type `RumblePatternParameter`. Inherits from `AtomVariableInstancer&lt;RumblePatternParameterVariable, RumblePatternParameterPair, RumblePatternParameter, RumblePatternParameterEvent, RumblePatternParameterPairEvent, RumblePatternParameterRumblePatternParameterFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-hotpink")]
    [AddComponentMenu("Unity Atoms/Variable Instancers/RumblePatternParameter Variable Instancer")]
    public class RumblePatternParameterVariableInstancer : AtomVariableInstancer<
        RumblePatternParameterVariable,
        RumblePatternParameterPair,
        RumblePatternParameter,
        RumblePatternParameterEvent,
        RumblePatternParameterPairEvent,
        RumblePatternParameterRumblePatternParameterFunction>
    { }
}
