namespace UnityAtoms
{
    /// <summary>
    /// Action of type `RumblePatternParameterPair`. Inherits from `AtomAction&lt;RumblePatternParameterPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class RumblePatternParameterPairAction : AtomAction<RumblePatternParameterPair> { }
}
