using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Set variable value Action of type `RumblePatternParameter`. Inherits from `SetVariableValue&lt;RumblePatternParameter, RumblePatternParameterPair, RumblePatternParameterVariable, RumblePatternParameterConstant, RumblePatternParameterReference, RumblePatternParameterEvent, RumblePatternParameterPairEvent, RumblePatternParameterVariableInstancer&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    [CreateAssetMenu(menuName = "Unity Atoms/Actions/Set Variable Value/RumblePatternParameter", fileName = "SetRumblePatternParameterVariableValue")]
    public sealed class SetRumblePatternParameterVariableValue : SetVariableValue<
        RumblePatternParameter,
        RumblePatternParameterPair,
        RumblePatternParameterVariable,
        RumblePatternParameterConstant,
        RumblePatternParameterReference,
        RumblePatternParameterEvent,
        RumblePatternParameterPairEvent,
        RumblePatternParameterRumblePatternParameterFunction,
        RumblePatternParameterVariableInstancer>
    { }
}
