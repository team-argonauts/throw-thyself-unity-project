namespace UnityAtoms
{
    /// <summary>
    /// Action of type `RumblePatternParameter`. Inherits from `AtomAction&lt;RumblePatternParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class RumblePatternParameterAction : AtomAction<RumblePatternParameter> { }
}
