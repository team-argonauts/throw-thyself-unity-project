namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `RumblePatternParameter`. Inherits from `AtomFunction&lt;RumblePatternParameter, RumblePatternParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class RumblePatternParameterRumblePatternParameterFunction : AtomFunction<RumblePatternParameter, RumblePatternParameter> { }
}
