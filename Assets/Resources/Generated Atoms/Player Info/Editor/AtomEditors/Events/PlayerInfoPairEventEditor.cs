#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityEngine.UIElements;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `PlayerInfoPair`. Inherits from `AtomEventEditor&lt;PlayerInfoPair, PlayerInfoPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomEditor(typeof(PlayerInfoPairEvent))]
    public sealed class PlayerInfoPairEventEditor : AtomEventEditor<PlayerInfoPair, PlayerInfoPairEvent> { }
}
#endif
