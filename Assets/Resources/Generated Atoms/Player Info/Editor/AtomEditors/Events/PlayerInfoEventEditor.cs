#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityEngine.UIElements;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `PlayerInfo`. Inherits from `AtomEventEditor&lt;PlayerInfo, PlayerInfoEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomEditor(typeof(PlayerInfoEvent))]
    public sealed class PlayerInfoEventEditor : AtomEventEditor<PlayerInfo, PlayerInfoEvent> { }
}
#endif
