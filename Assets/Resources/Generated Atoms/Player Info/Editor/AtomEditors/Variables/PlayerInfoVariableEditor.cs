using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable Inspector of type `PlayerInfo`. Inherits from `AtomVariableEditor`
    /// </summary>
    [CustomEditor(typeof(PlayerInfoVariable))]
    public sealed class PlayerInfoVariableEditor : AtomVariableEditor<PlayerInfo, PlayerInfoPair> { }
}
