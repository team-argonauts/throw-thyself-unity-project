#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Constant property drawer of type `PlayerInfo`. Inherits from `AtomDrawer&lt;PlayerInfoConstant&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(PlayerInfoConstant))]
    public class PlayerInfoConstantDrawer : VariableDrawer<PlayerInfoConstant> { }
}
#endif
