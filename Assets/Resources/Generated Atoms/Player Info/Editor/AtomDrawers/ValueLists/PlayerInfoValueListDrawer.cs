#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Value List property drawer of type `PlayerInfo`. Inherits from `AtomDrawer&lt;PlayerInfoValueList&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(PlayerInfoValueList))]
    public class PlayerInfoValueListDrawer : AtomDrawer<PlayerInfoValueList> { }
}
#endif
