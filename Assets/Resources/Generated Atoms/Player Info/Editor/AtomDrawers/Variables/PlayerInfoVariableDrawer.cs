#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable property drawer of type `PlayerInfo`. Inherits from `AtomDrawer&lt;PlayerInfoVariable&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(PlayerInfoVariable))]
    public class PlayerInfoVariableDrawer : VariableDrawer<PlayerInfoVariable> { }
}
#endif
