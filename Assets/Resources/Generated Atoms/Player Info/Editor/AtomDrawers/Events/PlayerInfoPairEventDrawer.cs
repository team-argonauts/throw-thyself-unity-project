#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `PlayerInfoPair`. Inherits from `AtomDrawer&lt;PlayerInfoPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(PlayerInfoPairEvent))]
    public class PlayerInfoPairEventDrawer : AtomDrawer<PlayerInfoPairEvent> { }
}
#endif
