using System;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Reference of type `PlayerInfo`. Inherits from `AtomReference&lt;PlayerInfo, PlayerInfoPair, PlayerInfoConstant, PlayerInfoVariable, PlayerInfoEvent, PlayerInfoPairEvent, PlayerInfoPlayerInfoFunction, PlayerInfoVariableInstancer, AtomCollection, AtomList&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerInfoReference : AtomReference<
        PlayerInfo,
        PlayerInfoPair,
        PlayerInfoConstant,
        PlayerInfoVariable,
        PlayerInfoEvent,
        PlayerInfoPairEvent,
        PlayerInfoPlayerInfoFunction,
        PlayerInfoVariableInstancer>, IEquatable<PlayerInfoReference>
    {
        public PlayerInfoReference() : base() { }
        public PlayerInfoReference(PlayerInfo value) : base(value) { }
        public bool Equals(PlayerInfoReference other) { return base.Equals(other); }
        protected override bool ValueEquals(PlayerInfo other)
        {
            throw new NotImplementedException();
        }
    }
}
