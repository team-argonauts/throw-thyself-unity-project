using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `PlayerInfoPair`. Inherits from `UltEvent&lt;PlayerInfoPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerInfoPairUltEvent : UltEvent<PlayerInfoPair> { }
}
