using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Constant of type `PlayerInfo`. Inherits from `AtomBaseVariable&lt;PlayerInfo&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-teal")]
    [CreateAssetMenu(menuName = "Unity Atoms/Constants/PlayerInfo", fileName = "PlayerInfoConstant")]
    public sealed class PlayerInfoConstant : AtomBaseVariable<PlayerInfo> { }
}
