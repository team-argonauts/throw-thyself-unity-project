using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `PlayerInfo`. Inherits from `AtomEventReferenceListener&lt;PlayerInfo, PlayerInfoEvent, PlayerInfoEventReference, PlayerInfoUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/PlayerInfo Event Reference Listener")]
    public sealed class PlayerInfoEventReferenceListener : AtomEventReferenceListener<
        PlayerInfo,
        PlayerInfoEvent,
        PlayerInfoEventReference,
        PlayerInfoUltEvent>
    { }
}
