using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `PlayerInfoPair`. Inherits from `AtomEventReferenceListener&lt;PlayerInfoPair, PlayerInfoPairEvent, PlayerInfoPairEventReference, PlayerInfoPairUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/PlayerInfoPair Event Reference Listener")]
    public sealed class PlayerInfoPairEventReferenceListener : AtomEventReferenceListener<
        PlayerInfoPair,
        PlayerInfoPairEvent,
        PlayerInfoPairEventReference,
        PlayerInfoPairUltEvent>
    { }
}
