using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Variable Instancer of type `PlayerInfo`. Inherits from `AtomVariableInstancer&lt;PlayerInfoVariable, PlayerInfoPair, PlayerInfo, PlayerInfoEvent, PlayerInfoPairEvent, PlayerInfoPlayerInfoFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-hotpink")]
    [AddComponentMenu("Unity Atoms/Variable Instancers/PlayerInfo Variable Instancer")]
    public class PlayerInfoVariableInstancer : AtomVariableInstancer<
        PlayerInfoVariable,
        PlayerInfoPair,
        PlayerInfo,
        PlayerInfoEvent,
        PlayerInfoPairEvent,
        PlayerInfoPlayerInfoFunction>
    { }
}
