using System;
using UnityEngine;
namespace UnityAtoms
{
    /// <summary>
    /// IPair of type `&lt;PlayerInfo&gt;`. Inherits from `IPair&lt;PlayerInfo&gt;`.
    /// </summary>
    [Serializable]
    public struct PlayerInfoPair : IPair<PlayerInfo>
    {
        public PlayerInfo Item1 { get => _item1; set => _item1 = value; }
        public PlayerInfo Item2 { get => _item2; set => _item2 = value; }

        [SerializeField]
        private PlayerInfo _item1;
        [SerializeField]
        private PlayerInfo _item2;

        public void Deconstruct(out PlayerInfo item1, out PlayerInfo item2) { item1 = Item1; item2 = Item2; }
    }
}