using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Value List of type `PlayerInfo`. Inherits from `AtomValueList&lt;PlayerInfo, PlayerInfoEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-piglet")]
    [CreateAssetMenu(menuName = "Unity Atoms/Value Lists/PlayerInfo", fileName = "PlayerInfoValueList")]
    public sealed class PlayerInfoValueList : AtomValueList<PlayerInfo, PlayerInfoEvent> { }
}
