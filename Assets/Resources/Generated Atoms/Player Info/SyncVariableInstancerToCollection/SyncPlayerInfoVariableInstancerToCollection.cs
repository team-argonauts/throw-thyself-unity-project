using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Adds Variable Instancer's Variable of type PlayerInfo to a Collection or List on OnEnable and removes it on OnDestroy. 
    /// </summary>
    [AddComponentMenu("Unity Atoms/Sync Variable Instancer to Collection/Sync PlayerInfo Variable Instancer to Collection")]
    [EditorIcon("atom-icon-delicate")]
    public class SyncPlayerInfoVariableInstancerToCollection : SyncVariableInstancerToCollection<PlayerInfo, PlayerInfoVariable, PlayerInfoVariableInstancer> { }
}
