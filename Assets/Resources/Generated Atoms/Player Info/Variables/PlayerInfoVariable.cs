using UnityEngine;
using System;

namespace UnityAtoms
{
    /// <summary>
    /// Variable of type `PlayerInfo`. Inherits from `AtomVariable&lt;PlayerInfo, PlayerInfoPair, PlayerInfoEvent, PlayerInfoPairEvent, PlayerInfoPlayerInfoFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-lush")]
    [CreateAssetMenu(menuName = "Unity Atoms/Variables/PlayerInfo", fileName = "PlayerInfoVariable")]
    public sealed class PlayerInfoVariable : AtomVariable<PlayerInfo, PlayerInfoPair, PlayerInfoEvent, PlayerInfoPairEvent, PlayerInfoPlayerInfoFunction>
    {
        protected override bool ValueEquals(PlayerInfo other)
        {
            if (Value.PlayerColor != other.PlayerColor)
                return false;
            if (Value.PlayerPortraitSprite != other.PlayerPortraitSprite)
                return false;
            return true;
        }
    }
}
