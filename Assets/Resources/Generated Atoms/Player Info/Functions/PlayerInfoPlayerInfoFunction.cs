namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `PlayerInfo`. Inherits from `AtomFunction&lt;PlayerInfo, PlayerInfo&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class PlayerInfoPlayerInfoFunction : AtomFunction<PlayerInfo, PlayerInfo> { }
}
