using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `PlayerInfoPair`. Inherits from `AtomEventInstancer&lt;PlayerInfoPair, PlayerInfoPairEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/PlayerInfoPair Event Instancer")]
    public class PlayerInfoPairEventInstancer : AtomEventInstancer<PlayerInfoPair, PlayerInfoPairEvent> { }
}
