using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `PlayerInfo`. Inherits from `AtomEventInstancer&lt;PlayerInfo, PlayerInfoEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/PlayerInfo Event Instancer")]
    public class PlayerInfoEventInstancer : AtomEventInstancer<PlayerInfo, PlayerInfoEvent> { }
}
