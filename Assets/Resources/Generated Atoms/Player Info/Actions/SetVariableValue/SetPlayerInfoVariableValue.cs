using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Set variable value Action of type `PlayerInfo`. Inherits from `SetVariableValue&lt;PlayerInfo, PlayerInfoPair, PlayerInfoVariable, PlayerInfoConstant, PlayerInfoReference, PlayerInfoEvent, PlayerInfoPairEvent, PlayerInfoVariableInstancer&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    [CreateAssetMenu(menuName = "Unity Atoms/Actions/Set Variable Value/PlayerInfo", fileName = "SetPlayerInfoVariableValue")]
    public sealed class SetPlayerInfoVariableValue : SetVariableValue<
        PlayerInfo,
        PlayerInfoPair,
        PlayerInfoVariable,
        PlayerInfoConstant,
        PlayerInfoReference,
        PlayerInfoEvent,
        PlayerInfoPairEvent,
        PlayerInfoPlayerInfoFunction,
        PlayerInfoVariableInstancer>
    { }
}
