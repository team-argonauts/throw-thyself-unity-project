namespace UnityAtoms
{
    /// <summary>
    /// Action of type `PlayerInfo`. Inherits from `AtomAction&lt;PlayerInfo&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class PlayerInfoAction : AtomAction<PlayerInfo> { }
}
