namespace UnityAtoms
{
    /// <summary>
    /// Action of type `PlayerInfoPair`. Inherits from `AtomAction&lt;PlayerInfoPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class PlayerInfoPairAction : AtomAction<PlayerInfoPair> { }
}
