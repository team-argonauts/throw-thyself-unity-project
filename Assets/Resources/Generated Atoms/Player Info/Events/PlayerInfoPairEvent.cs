using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `PlayerInfoPair`. Inherits from `AtomEvent&lt;PlayerInfoPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/PlayerInfoPair", fileName = "PlayerInfoPairEvent")]
    public sealed class PlayerInfoPairEvent : AtomEvent<PlayerInfoPair>
    {
    }
}
