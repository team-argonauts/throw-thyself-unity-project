using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `PlayerInfo`. Inherits from `AtomEvent&lt;PlayerInfo&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/PlayerInfo", fileName = "PlayerInfoEvent")]
    public sealed class PlayerInfoEvent : AtomEvent<PlayerInfo>
    {
    }
}
