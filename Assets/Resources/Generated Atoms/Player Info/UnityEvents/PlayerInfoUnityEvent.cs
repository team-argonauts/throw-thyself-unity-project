using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `PlayerInfo`. Inherits from `UltEvent&lt;PlayerInfo&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerInfoUltEvent : UltEvent<PlayerInfo> { }
}
