using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `PlayerInfo`. Inherits from `AtomEventReference&lt;PlayerInfo, PlayerInfoVariable, PlayerInfoEvent, PlayerInfoVariableInstancer, PlayerInfoEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerInfoEventReference : AtomEventReference<
        PlayerInfo,
        PlayerInfoVariable,
        PlayerInfoEvent,
        PlayerInfoVariableInstancer,
        PlayerInfoEventInstancer>, IGetEvent 
    { }
}
