using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `PlayerInfoPair`. Inherits from `AtomEventReference&lt;PlayerInfoPair, PlayerInfoVariable, PlayerInfoPairEvent, PlayerInfoVariableInstancer, PlayerInfoPairEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class PlayerInfoPairEventReference : AtomEventReference<
        PlayerInfoPair,
        PlayerInfoVariable,
        PlayerInfoPairEvent,
        PlayerInfoVariableInstancer,
        PlayerInfoPairEventInstancer>, IGetEvent 
    { }
}
