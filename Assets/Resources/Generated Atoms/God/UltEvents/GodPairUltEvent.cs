using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `GodPair`. Inherits from `UltEvent&lt;GodPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GodPairUltEvent : UltEvent<GodPair> { }
}
