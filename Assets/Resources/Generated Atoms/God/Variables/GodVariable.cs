using UnityEngine;
using System;

namespace UnityAtoms
{
    /// <summary>
    /// Variable of type `God`. Inherits from `AtomVariable&lt;God, GodPair, GodEvent, GodPairEvent, GodGodFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-lush")]
    [CreateAssetMenu(menuName = "Unity Atoms/Variables/God", fileName = "GodVariable")]
    public sealed class GodVariable : AtomVariable<God, GodPair, GodEvent, GodPairEvent, GodGodFunction>
    {
        protected override bool ValueEquals(God other)
        {
            if (Value.GodObject != other.GodObject)
                return false;
            if (Value.FollowTarget != other.FollowTarget)
                return false;
            if (Value.WeaponBody != other.WeaponBody)
                return false;
            if (Value.RagdollExploder != other.RagdollExploder)
                return false;

            return true;
        }
    }
}
