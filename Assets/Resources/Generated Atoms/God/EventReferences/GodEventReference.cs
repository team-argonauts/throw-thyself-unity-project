using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `God`. Inherits from `AtomEventReference&lt;God, GodVariable, GodEvent, GodVariableInstancer, GodEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GodEventReference : AtomEventReference<
        God,
        GodVariable,
        GodEvent,
        GodVariableInstancer,
        GodEventInstancer>, IGetEvent 
    { }
}
