using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `GodPair`. Inherits from `AtomEventReference&lt;GodPair, GodVariable, GodPairEvent, GodVariableInstancer, GodPairEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GodPairEventReference : AtomEventReference<
        GodPair,
        GodVariable,
        GodPairEvent,
        GodVariableInstancer,
        GodPairEventInstancer>, IGetEvent 
    { }
}
