using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `God`. Inherits from `AtomEventInstancer&lt;God, GodEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/God Event Instancer")]
    public class GodEventInstancer : AtomEventInstancer<God, GodEvent> { }
}
