using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `GodPair`. Inherits from `AtomEventInstancer&lt;GodPair, GodPairEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/GodPair Event Instancer")]
    public class GodPairEventInstancer : AtomEventInstancer<GodPair, GodPairEvent> { }
}
