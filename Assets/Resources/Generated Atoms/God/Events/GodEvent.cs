using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `God`. Inherits from `AtomEvent&lt;God&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/God", fileName = "GodEvent")]
    public sealed class GodEvent : AtomEvent<God>
    {
    }
}
