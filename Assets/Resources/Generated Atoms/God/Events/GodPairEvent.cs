using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `GodPair`. Inherits from `AtomEvent&lt;GodPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/GodPair", fileName = "GodPairEvent")]
    public sealed class GodPairEvent : AtomEvent<GodPair>
    {
    }
}
