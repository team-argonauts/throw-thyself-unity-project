using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `God`. Inherits from `UltEvent&lt;God&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GodUltEvent : UltEvent<God> { }
}
