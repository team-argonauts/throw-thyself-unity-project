using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Constant of type `God`. Inherits from `AtomBaseVariable&lt;God&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-teal")]
    [CreateAssetMenu(menuName = "Unity Atoms/Constants/God", fileName = "GodConstant")]
    public sealed class GodConstant : AtomBaseVariable<God> { }
}
