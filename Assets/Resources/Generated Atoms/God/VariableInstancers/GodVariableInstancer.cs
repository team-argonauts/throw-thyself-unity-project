using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Variable Instancer of type `God`. Inherits from `AtomVariableInstancer&lt;GodVariable, GodPair, God, GodEvent, GodPairEvent, GodGodFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-hotpink")]
    [AddComponentMenu("Unity Atoms/Variable Instancers/God Variable Instancer")]
    public class GodVariableInstancer : AtomVariableInstancer<
        GodVariable,
        GodPair,
        God,
        GodEvent,
        GodPairEvent,
        GodGodFunction>
    { }
}
