namespace UnityAtoms
{
    /// <summary>
    /// Action of type `GodPair`. Inherits from `AtomAction&lt;GodPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class GodPairAction : AtomAction<GodPair> { }
}
