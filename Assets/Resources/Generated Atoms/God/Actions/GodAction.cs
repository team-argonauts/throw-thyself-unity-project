namespace UnityAtoms
{
    /// <summary>
    /// Action of type `God`. Inherits from `AtomAction&lt;God&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class GodAction : AtomAction<God> { }
}
