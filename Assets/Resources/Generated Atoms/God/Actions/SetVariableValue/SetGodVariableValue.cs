using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Set variable value Action of type `God`. Inherits from `SetVariableValue&lt;God, GodPair, GodVariable, GodConstant, GodReference, GodEvent, GodPairEvent, GodVariableInstancer&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    [CreateAssetMenu(menuName = "Unity Atoms/Actions/Set Variable Value/God", fileName = "SetGodVariableValue")]
    public sealed class SetGodVariableValue : SetVariableValue<
        God,
        GodPair,
        GodVariable,
        GodConstant,
        GodReference,
        GodEvent,
        GodPairEvent,
        GodGodFunction,
        GodVariableInstancer>
    { }
}
