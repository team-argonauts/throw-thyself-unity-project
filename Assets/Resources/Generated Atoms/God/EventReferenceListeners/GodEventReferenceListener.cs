using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `God`. Inherits from `AtomEventReferenceListener&lt;God, GodEvent, GodEventReference, GodUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/God Event Reference Listener")]
    public sealed class GodEventReferenceListener : AtomEventReferenceListener<
        God,
        GodEvent,
        GodEventReference,
        GodUltEvent>
    { }
}
