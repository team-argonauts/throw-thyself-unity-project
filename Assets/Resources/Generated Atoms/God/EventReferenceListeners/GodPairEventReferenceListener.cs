using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `GodPair`. Inherits from `AtomEventReferenceListener&lt;GodPair, GodPairEvent, GodPairEventReference, GodPairUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/GodPair Event Reference Listener")]
    public sealed class GodPairEventReferenceListener : AtomEventReferenceListener<
        GodPair,
        GodPairEvent,
        GodPairEventReference,
        GodPairUltEvent>
    { }
}
