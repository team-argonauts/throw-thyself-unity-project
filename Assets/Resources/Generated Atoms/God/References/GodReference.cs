using System;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Reference of type `God`. Inherits from `AtomReference&lt;God, GodPair, GodConstant, GodVariable, GodEvent, GodPairEvent, GodGodFunction, GodVariableInstancer, AtomCollection, AtomList&gt;`.
    /// </summary>
    [Serializable]
    public sealed class GodReference : AtomReference<
        God,
        GodPair,
        GodConstant,
        GodVariable,
        GodEvent,
        GodPairEvent,
        GodGodFunction,
        GodVariableInstancer>, IEquatable<GodReference>
    {
        public GodReference() : base() { }
        public GodReference(God value) : base(value) { }
        public bool Equals(GodReference other) { return base.Equals(other); }
        protected override bool ValueEquals(God other)
        {
            throw new NotImplementedException();
        }
    }
}
