using System;
using UnityEngine;
namespace UnityAtoms
{
    /// <summary>
    /// IPair of type `&lt;God&gt;`. Inherits from `IPair&lt;God&gt;`.
    /// </summary>
    [Serializable]
    public struct GodPair : IPair<God>
    {
        public God Item1 { get => _item1; set => _item1 = value; }
        public God Item2 { get => _item2; set => _item2 = value; }

        [SerializeField]
        private God _item1;
        [SerializeField]
        private God _item2;

        public void Deconstruct(out God item1, out God item2) { item1 = Item1; item2 = Item2; }
    }
}