namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `God`. Inherits from `AtomFunction&lt;God, God&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class GodGodFunction : AtomFunction<God, God> { }
}
