using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable Inspector of type `God`. Inherits from `AtomVariableEditor`
    /// </summary>
    [CustomEditor(typeof(GodVariable))]
    public sealed class GodVariableEditor : AtomVariableEditor<God, GodPair> { }
}
