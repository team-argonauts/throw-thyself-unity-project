#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityEngine.UIElements;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `GodPair`. Inherits from `AtomEventEditor&lt;GodPair, GodPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomEditor(typeof(GodPairEvent))]
    public sealed class GodPairEventEditor : AtomEventEditor<GodPair, GodPairEvent> { }
}
#endif
