#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Value List property drawer of type `God`. Inherits from `AtomDrawer&lt;GodValueList&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(GodValueList))]
    public class GodValueListDrawer : AtomDrawer<GodValueList> { }
}
#endif
