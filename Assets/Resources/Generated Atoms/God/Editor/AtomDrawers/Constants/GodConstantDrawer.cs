#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Constant property drawer of type `God`. Inherits from `AtomDrawer&lt;GodConstant&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(GodConstant))]
    public class GodConstantDrawer : VariableDrawer<GodConstant> { }
}
#endif
