#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable property drawer of type `God`. Inherits from `AtomDrawer&lt;GodVariable&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(GodVariable))]
    public class GodVariableDrawer : VariableDrawer<GodVariable> { }
}
#endif
