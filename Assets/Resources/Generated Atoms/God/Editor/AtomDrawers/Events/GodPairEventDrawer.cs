#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `GodPair`. Inherits from `AtomDrawer&lt;GodPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(GodPairEvent))]
    public class GodPairEventDrawer : AtomDrawer<GodPairEvent> { }
}
#endif
