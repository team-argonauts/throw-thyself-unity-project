using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Value List of type `God`. Inherits from `AtomValueList&lt;God, GodEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-piglet")]
    [CreateAssetMenu(menuName = "Unity Atoms/Value Lists/God", fileName = "GodValueList")]
    public sealed class GodValueList : AtomValueList<God, GodEvent> { }
}
