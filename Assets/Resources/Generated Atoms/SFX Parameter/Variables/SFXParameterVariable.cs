using UnityEngine;
using System;

namespace UnityAtoms
{
    /// <summary>
    /// Variable of type `SFXParameter`. Inherits from `AtomVariable&lt;SFXParameter, SFXParameterPair, SFXParameterEvent, SFXParameterPairEvent, SFXParameterSFXParameterFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-lush")]
    [CreateAssetMenu(menuName = "Unity Atoms/Variables/SFXParameter", fileName = "SFXParameterVariable")]
    public sealed class SFXParameterVariable : AtomVariable<SFXParameter, SFXParameterPair, SFXParameterEvent, SFXParameterPairEvent, SFXParameterSFXParameterFunction>
    {
        protected override bool ValueEquals(SFXParameter other)
        {
            if (Value.AudioClip != other.AudioClip)
                return false;
            if (Value.Volume != other.Volume)
                return false;

            return true;
        }
    }
}
