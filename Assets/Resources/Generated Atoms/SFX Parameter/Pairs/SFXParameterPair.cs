using System;
using UnityEngine;
namespace UnityAtoms
{
    /// <summary>
    /// IPair of type `&lt;SFXParameter&gt;`. Inherits from `IPair&lt;SFXParameter&gt;`.
    /// </summary>
    [Serializable]
    public struct SFXParameterPair : IPair<SFXParameter>
    {
        public SFXParameter Item1 { get => _item1; set => _item1 = value; }
        public SFXParameter Item2 { get => _item2; set => _item2 = value; }

        [SerializeField]
        private SFXParameter _item1;
        [SerializeField]
        private SFXParameter _item2;

        public void Deconstruct(out SFXParameter item1, out SFXParameter item2) { item1 = Item1; item2 = Item2; }
    }
}