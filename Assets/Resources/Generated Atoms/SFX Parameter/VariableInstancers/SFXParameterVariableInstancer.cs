using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Variable Instancer of type `SFXParameter`. Inherits from `AtomVariableInstancer&lt;SFXParameterVariable, SFXParameterPair, SFXParameter, SFXParameterEvent, SFXParameterPairEvent, SFXParameterSFXParameterFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-hotpink")]
    [AddComponentMenu("Unity Atoms/Variable Instancers/SFXParameter Variable Instancer")]
    public class SFXParameterVariableInstancer : AtomVariableInstancer<
        SFXParameterVariable,
        SFXParameterPair,
        SFXParameter,
        SFXParameterEvent,
        SFXParameterPairEvent,
        SFXParameterSFXParameterFunction>
    { }
}
