using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Set variable value Action of type `SFXParameter`. Inherits from `SetVariableValue&lt;SFXParameter, SFXParameterPair, SFXParameterVariable, SFXParameterConstant, SFXParameterReference, SFXParameterEvent, SFXParameterPairEvent, SFXParameterVariableInstancer&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    [CreateAssetMenu(menuName = "Unity Atoms/Actions/Set Variable Value/SFXParameter", fileName = "SetSFXParameterVariableValue")]
    public sealed class SetSFXParameterVariableValue : SetVariableValue<
        SFXParameter,
        SFXParameterPair,
        SFXParameterVariable,
        SFXParameterConstant,
        SFXParameterReference,
        SFXParameterEvent,
        SFXParameterPairEvent,
        SFXParameterSFXParameterFunction,
        SFXParameterVariableInstancer>
    { }
}
