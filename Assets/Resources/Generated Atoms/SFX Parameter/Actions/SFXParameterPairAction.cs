namespace UnityAtoms
{
    /// <summary>
    /// Action of type `SFXParameterPair`. Inherits from `AtomAction&lt;SFXParameterPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class SFXParameterPairAction : AtomAction<SFXParameterPair> { }
}
