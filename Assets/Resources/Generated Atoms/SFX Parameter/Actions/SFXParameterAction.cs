namespace UnityAtoms
{
    /// <summary>
    /// Action of type `SFXParameter`. Inherits from `AtomAction&lt;SFXParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class SFXParameterAction : AtomAction<SFXParameter> { }
}
