using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `SFXParameter`. Inherits from `AtomEventReferenceListener&lt;SFXParameter, SFXParameterEvent, SFXParameterEventReference, SFXParameterUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/SFXParameter Event Reference Listener")]
    public sealed class SFXParameterEventReferenceListener : AtomEventReferenceListener<
        SFXParameter,
        SFXParameterEvent,
        SFXParameterEventReference,
        SFXParameterUltEvent>
    { }
}
