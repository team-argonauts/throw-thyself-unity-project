using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `SFXParameterPair`. Inherits from `AtomEventReferenceListener&lt;SFXParameterPair, SFXParameterPairEvent, SFXParameterPairEventReference, SFXParameterPairUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/SFXParameterPair Event Reference Listener")]
    public sealed class SFXParameterPairEventReferenceListener : AtomEventReferenceListener<
        SFXParameterPair,
        SFXParameterPairEvent,
        SFXParameterPairEventReference,
        SFXParameterPairUltEvent>
    { }
}
