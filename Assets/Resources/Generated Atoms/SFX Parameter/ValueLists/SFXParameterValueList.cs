using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Value List of type `SFXParameter`. Inherits from `AtomValueList&lt;SFXParameter, SFXParameterEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-piglet")]
    [CreateAssetMenu(menuName = "Unity Atoms/Value Lists/SFXParameter", fileName = "SFXParameterValueList")]
    public sealed class SFXParameterValueList : AtomValueList<SFXParameter, SFXParameterEvent> { }
}
