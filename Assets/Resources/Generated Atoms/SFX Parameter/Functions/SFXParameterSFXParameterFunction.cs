namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `SFXParameter`. Inherits from `AtomFunction&lt;SFXParameter, SFXParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class SFXParameterSFXParameterFunction : AtomFunction<SFXParameter, SFXParameter> { }
}
