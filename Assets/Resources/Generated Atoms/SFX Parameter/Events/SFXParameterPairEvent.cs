using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `SFXParameterPair`. Inherits from `AtomEvent&lt;SFXParameterPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/SFXParameterPair", fileName = "SFXParameterPairEvent")]
    public sealed class SFXParameterPairEvent : AtomEvent<SFXParameterPair>
    {
    }
}
