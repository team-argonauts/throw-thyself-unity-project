using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `SFXParameter`. Inherits from `AtomEvent&lt;SFXParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/SFXParameter", fileName = "SFXParameterEvent")]
    public sealed class SFXParameterEvent : AtomEvent<SFXParameter>
    {
    }
}
