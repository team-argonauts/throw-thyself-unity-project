using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `SFXParameter`. Inherits from `UltEvent&lt;SFXParameter&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SFXParameterUltEvent : UltEvent<SFXParameter> { }
}
