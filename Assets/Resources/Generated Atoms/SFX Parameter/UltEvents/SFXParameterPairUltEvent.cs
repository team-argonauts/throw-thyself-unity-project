using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Ult Event of type `SFXParameterPair`. Inherits from `UltEvent&lt;SFXParameterPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SFXParameterPairUltEvent : UltEvent<SFXParameterPair> { }
}
