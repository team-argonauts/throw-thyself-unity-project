using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Constant of type `SFXParameter`. Inherits from `AtomBaseVariable&lt;SFXParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-teal")]
    [CreateAssetMenu(menuName = "Unity Atoms/Constants/SFXParameter", fileName = "SFXParameterConstant")]
    public sealed class SFXParameterConstant : AtomBaseVariable<SFXParameter> { }
}
