using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `SFXParameter`. Inherits from `AtomEventInstancer&lt;SFXParameter, SFXParameterEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/SFXParameter Event Instancer")]
    public class SFXParameterEventInstancer : AtomEventInstancer<SFXParameter, SFXParameterEvent> { }
}
