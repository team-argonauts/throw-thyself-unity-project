using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `SFXParameterPair`. Inherits from `AtomEventInstancer&lt;SFXParameterPair, SFXParameterPairEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/SFXParameterPair Event Instancer")]
    public class SFXParameterPairEventInstancer : AtomEventInstancer<SFXParameterPair, SFXParameterPairEvent> { }
}
