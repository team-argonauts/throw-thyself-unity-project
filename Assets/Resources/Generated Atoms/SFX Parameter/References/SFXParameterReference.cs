using System;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Reference of type `SFXParameter`. Inherits from `AtomReference&lt;SFXParameter, SFXParameterPair, SFXParameterConstant, SFXParameterVariable, SFXParameterEvent, SFXParameterPairEvent, SFXParameterSFXParameterFunction, SFXParameterVariableInstancer, AtomCollection, AtomList&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SFXParameterReference : AtomReference<
        SFXParameter,
        SFXParameterPair,
        SFXParameterConstant,
        SFXParameterVariable,
        SFXParameterEvent,
        SFXParameterPairEvent,
        SFXParameterSFXParameterFunction,
        SFXParameterVariableInstancer>, IEquatable<SFXParameterReference>
    {
        public SFXParameterReference() : base() { }
        public SFXParameterReference(SFXParameter value) : base(value) { }
        public bool Equals(SFXParameterReference other) { return base.Equals(other); }
        protected override bool ValueEquals(SFXParameter other)
        {
            throw new NotImplementedException();
        }
    }
}
