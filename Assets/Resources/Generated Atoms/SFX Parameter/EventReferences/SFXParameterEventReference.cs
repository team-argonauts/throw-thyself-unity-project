using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `SFXParameter`. Inherits from `AtomEventReference&lt;SFXParameter, SFXParameterVariable, SFXParameterEvent, SFXParameterVariableInstancer, SFXParameterEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SFXParameterEventReference : AtomEventReference<
        SFXParameter,
        SFXParameterVariable,
        SFXParameterEvent,
        SFXParameterVariableInstancer,
        SFXParameterEventInstancer>, IGetEvent 
    { }
}
