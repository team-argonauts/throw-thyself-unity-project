using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `SFXParameterPair`. Inherits from `AtomEventReference&lt;SFXParameterPair, SFXParameterVariable, SFXParameterPairEvent, SFXParameterVariableInstancer, SFXParameterPairEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class SFXParameterPairEventReference : AtomEventReference<
        SFXParameterPair,
        SFXParameterVariable,
        SFXParameterPairEvent,
        SFXParameterVariableInstancer,
        SFXParameterPairEventInstancer>, IGetEvent 
    { }
}
