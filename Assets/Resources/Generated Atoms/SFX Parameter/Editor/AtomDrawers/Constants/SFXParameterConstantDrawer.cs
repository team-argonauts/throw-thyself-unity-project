#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Constant property drawer of type `SFXParameter`. Inherits from `AtomDrawer&lt;SFXParameterConstant&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(SFXParameterConstant))]
    public class SFXParameterConstantDrawer : VariableDrawer<SFXParameterConstant> { }
}
#endif
