#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Value List property drawer of type `SFXParameter`. Inherits from `AtomDrawer&lt;SFXParameterValueList&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(SFXParameterValueList))]
    public class SFXParameterValueListDrawer : AtomDrawer<SFXParameterValueList> { }
}
#endif
