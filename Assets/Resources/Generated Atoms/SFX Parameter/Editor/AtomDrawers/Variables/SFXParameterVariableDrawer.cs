#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable property drawer of type `SFXParameter`. Inherits from `AtomDrawer&lt;SFXParameterVariable&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(SFXParameterVariable))]
    public class SFXParameterVariableDrawer : VariableDrawer<SFXParameterVariable> { }
}
#endif
