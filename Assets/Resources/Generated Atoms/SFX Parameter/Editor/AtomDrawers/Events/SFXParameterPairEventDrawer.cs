#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `SFXParameterPair`. Inherits from `AtomDrawer&lt;SFXParameterPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(SFXParameterPairEvent))]
    public class SFXParameterPairEventDrawer : AtomDrawer<SFXParameterPairEvent> { }
}
#endif
