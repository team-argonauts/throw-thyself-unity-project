#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `SFXParameter`. Inherits from `AtomDrawer&lt;SFXParameterEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(SFXParameterEvent))]
    public class SFXParameterEventDrawer : AtomDrawer<SFXParameterEvent> { }
}
#endif
