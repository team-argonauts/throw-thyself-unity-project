#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityEngine.UIElements;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `SFXParameter`. Inherits from `AtomEventEditor&lt;SFXParameter, SFXParameterEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomEditor(typeof(SFXParameterEvent))]
    public sealed class SFXParameterEventEditor : AtomEventEditor<SFXParameter, SFXParameterEvent> { }
}
#endif
