using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable Inspector of type `SFXParameter`. Inherits from `AtomVariableEditor`
    /// </summary>
    [CustomEditor(typeof(SFXParameterVariable))]
    public sealed class SFXParameterVariableEditor : AtomVariableEditor<SFXParameter, SFXParameterPair> { }
}
