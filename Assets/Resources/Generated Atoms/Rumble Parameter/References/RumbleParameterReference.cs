using System;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Reference of type `RumbleParameter`. Inherits from `AtomReference&lt;RumbleParameter, RumbleParameterPair, RumbleParameterConstant, RumbleParameterVariable, RumbleParameterEvent, RumbleParameterPairEvent, RumbleParameterRumbleParameterFunction, RumbleParameterVariableInstancer, AtomCollection, AtomList&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumbleParameterReference : AtomReference<
        RumbleParameter,
        RumbleParameterPair,
        RumbleParameterConstant,
        RumbleParameterVariable,
        RumbleParameterEvent,
        RumbleParameterPairEvent,
        RumbleParameterRumbleParameterFunction,
        RumbleParameterVariableInstancer>, IEquatable<RumbleParameterReference>
    {
        public RumbleParameterReference() : base() { }
        public RumbleParameterReference(RumbleParameter value) : base(value) { }
        public bool Equals(RumbleParameterReference other) { return base.Equals(other); }
        protected override bool ValueEquals(RumbleParameter other)
        {
            throw new NotImplementedException();
        }
    }
}
