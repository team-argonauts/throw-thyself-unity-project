using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `RumbleParameter`. Inherits from `AtomEventReference&lt;RumbleParameter, RumbleParameterVariable, RumbleParameterEvent, RumbleParameterVariableInstancer, RumbleParameterEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumbleParameterEventReference : AtomEventReference<
        RumbleParameter,
        RumbleParameterVariable,
        RumbleParameterEvent,
        RumbleParameterVariableInstancer,
        RumbleParameterEventInstancer>, IGetEvent 
    { }
}
