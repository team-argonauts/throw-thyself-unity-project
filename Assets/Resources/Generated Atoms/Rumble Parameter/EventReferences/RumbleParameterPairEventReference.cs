using System;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference of type `RumbleParameterPair`. Inherits from `AtomEventReference&lt;RumbleParameterPair, RumbleParameterVariable, RumbleParameterPairEvent, RumbleParameterVariableInstancer, RumbleParameterPairEventInstancer&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumbleParameterPairEventReference : AtomEventReference<
        RumbleParameterPair,
        RumbleParameterVariable,
        RumbleParameterPairEvent,
        RumbleParameterVariableInstancer,
        RumbleParameterPairEventInstancer>, IGetEvent 
    { }
}
