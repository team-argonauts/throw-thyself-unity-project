using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Unity Event of type `RumbleParameter`. Inherits from `UltEvent&lt;RumbleParameter&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumbleParameterUltEvent : UltEvent<RumbleParameter> { }
}
