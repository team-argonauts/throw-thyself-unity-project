using System;
using UltEvents;

namespace UnityAtoms
{
    /// <summary>
    /// None generic Unity Event of type `RumbleParameterPair`. Inherits from `UltEvent&lt;RumbleParameterPair&gt;`.
    /// </summary>
    [Serializable]
    public sealed class RumbleParameterPairUltEvent : UltEvent<RumbleParameterPair> { }
}
