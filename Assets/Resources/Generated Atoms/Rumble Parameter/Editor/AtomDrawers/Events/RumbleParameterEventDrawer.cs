#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `RumbleParameter`. Inherits from `AtomDrawer&lt;RumbleParameterEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumbleParameterEvent))]
    public class RumbleParameterEventDrawer : AtomDrawer<RumbleParameterEvent> { }
}
#endif
