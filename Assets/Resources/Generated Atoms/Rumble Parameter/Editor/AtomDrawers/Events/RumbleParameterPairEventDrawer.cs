#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `RumbleParameterPair`. Inherits from `AtomDrawer&lt;RumbleParameterPairEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumbleParameterPairEvent))]
    public class RumbleParameterPairEventDrawer : AtomDrawer<RumbleParameterPairEvent> { }
}
#endif
