#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Constant property drawer of type `RumbleParameter`. Inherits from `AtomDrawer&lt;RumbleParameterConstant&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumbleParameterConstant))]
    public class RumbleParameterConstantDrawer : VariableDrawer<RumbleParameterConstant> { }
}
#endif
