#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable property drawer of type `RumbleParameter`. Inherits from `AtomDrawer&lt;RumbleParameterVariable&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomPropertyDrawer(typeof(RumbleParameterVariable))]
    public class RumbleParameterVariableDrawer : VariableDrawer<RumbleParameterVariable> { }
}
#endif
