using UnityEditor;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Variable Inspector of type `RumbleParameter`. Inherits from `AtomVariableEditor`
    /// </summary>
    [CustomEditor(typeof(RumbleParameterVariable))]
    public sealed class RumbleParameterVariableEditor : AtomVariableEditor<RumbleParameter, RumbleParameterPair> { }
}
