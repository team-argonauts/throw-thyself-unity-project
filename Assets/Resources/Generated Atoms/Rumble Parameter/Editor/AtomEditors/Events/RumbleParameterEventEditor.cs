#if UNITY_2019_1_OR_NEWER
using UnityEditor;
using UnityEngine.UIElements;
using UnityAtoms.Editor;

namespace UnityAtoms.Editor
{
    /// <summary>
    /// Event property drawer of type `RumbleParameter`. Inherits from `AtomEventEditor&lt;RumbleParameter, RumbleParameterEvent&gt;`. Only availble in `UNITY_2019_1_OR_NEWER`.
    /// </summary>
    [CustomEditor(typeof(RumbleParameterEvent))]
    public sealed class RumbleParameterEventEditor : AtomEventEditor<RumbleParameter, RumbleParameterEvent> { }
}
#endif
