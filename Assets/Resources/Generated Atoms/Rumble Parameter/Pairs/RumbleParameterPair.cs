using System;
using UnityEngine;
namespace UnityAtoms
{
    /// <summary>
    /// IPair of type `&lt;RumbleParameter&gt;`. Inherits from `IPair&lt;RumbleParameter&gt;`.
    /// </summary>
    [Serializable]
    public struct RumbleParameterPair : IPair<RumbleParameter>
    {
        public RumbleParameter Item1 { get => _item1; set => _item1 = value; }
        public RumbleParameter Item2 { get => _item2; set => _item2 = value; }

        [SerializeField]
        private RumbleParameter _item1;
        [SerializeField]
        private RumbleParameter _item2;

        public void Deconstruct(out RumbleParameter item1, out RumbleParameter item2) { item1 = Item1; item2 = Item2; }
    }
}