using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Constant of type `RumbleParameter`. Inherits from `AtomBaseVariable&lt;RumbleParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-teal")]
    [CreateAssetMenu(menuName = "Unity Atoms/Constants/RumbleParameter", fileName = "RumbleParameterConstant")]
    public sealed class RumbleParameterConstant : AtomBaseVariable<RumbleParameter> { }
}
