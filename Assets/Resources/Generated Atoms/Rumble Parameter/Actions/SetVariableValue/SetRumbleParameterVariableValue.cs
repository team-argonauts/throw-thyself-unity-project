using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Set variable value Action of type `RumbleParameter`. Inherits from `SetVariableValue&lt;RumbleParameter, RumbleParameterPair, RumbleParameterVariable, RumbleParameterConstant, RumbleParameterReference, RumbleParameterEvent, RumbleParameterPairEvent, RumbleParameterVariableInstancer&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    [CreateAssetMenu(menuName = "Unity Atoms/Actions/Set Variable Value/RumbleParameter", fileName = "SetRumbleParameterVariableValue")]
    public sealed class SetRumbleParameterVariableValue : SetVariableValue<
        RumbleParameter,
        RumbleParameterPair,
        RumbleParameterVariable,
        RumbleParameterConstant,
        RumbleParameterReference,
        RumbleParameterEvent,
        RumbleParameterPairEvent,
        RumbleParameterRumbleParameterFunction,
        RumbleParameterVariableInstancer>
    { }
}
