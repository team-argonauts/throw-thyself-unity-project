namespace UnityAtoms
{
    /// <summary>
    /// Action of type `RumbleParameterPair`. Inherits from `AtomAction&lt;RumbleParameterPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class RumbleParameterPairAction : AtomAction<RumbleParameterPair> { }
}
