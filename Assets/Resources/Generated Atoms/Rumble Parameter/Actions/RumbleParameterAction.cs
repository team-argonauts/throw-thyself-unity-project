namespace UnityAtoms
{
    /// <summary>
    /// Action of type `RumbleParameter`. Inherits from `AtomAction&lt;RumbleParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-purple")]
    public abstract class RumbleParameterAction : AtomAction<RumbleParameter> { }
}
