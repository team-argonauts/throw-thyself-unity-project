using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `RumbleParameterPair`. Inherits from `AtomEventInstancer&lt;RumbleParameterPair, RumbleParameterPairEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/RumbleParameterPair Event Instancer")]
    public class RumbleParameterPairEventInstancer : AtomEventInstancer<RumbleParameterPair, RumbleParameterPairEvent> { }
}
