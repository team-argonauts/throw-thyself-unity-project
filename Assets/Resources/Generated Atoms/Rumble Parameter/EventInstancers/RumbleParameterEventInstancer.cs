using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Instancer of type `RumbleParameter`. Inherits from `AtomEventInstancer&lt;RumbleParameter, RumbleParameterEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sign-blue")]
    [AddComponentMenu("Unity Atoms/Event Instancers/RumbleParameter Event Instancer")]
    public class RumbleParameterEventInstancer : AtomEventInstancer<RumbleParameter, RumbleParameterEvent> { }
}
