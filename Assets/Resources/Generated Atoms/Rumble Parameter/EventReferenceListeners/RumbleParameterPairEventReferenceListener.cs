using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `RumbleParameterPair`. Inherits from `AtomEventReferenceListener&lt;RumbleParameterPair, RumbleParameterPairEvent, RumbleParameterPairEventReference, RumbleParameterPairUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/RumbleParameterPair Event Reference Listener")]
    public sealed class RumbleParameterPairEventReferenceListener : AtomEventReferenceListener<
        RumbleParameterPair,
        RumbleParameterPairEvent,
        RumbleParameterPairEventReference,
        RumbleParameterPairUltEvent>
    { }
}
