using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event Reference Listener of type `RumbleParameter`. Inherits from `AtomEventReferenceListener&lt;RumbleParameter, RumbleParameterEvent, RumbleParameterEventReference, RumbleParameterUltEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-orange")]
    [AddComponentMenu("Unity Atoms/Listeners/RumbleParameter Event Reference Listener")]
    public sealed class RumbleParameterEventReferenceListener : AtomEventReferenceListener<
        RumbleParameter,
        RumbleParameterEvent,
        RumbleParameterEventReference,
        RumbleParameterUltEvent>
    { }
}
