using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Value List of type `RumbleParameter`. Inherits from `AtomValueList&lt;RumbleParameter, RumbleParameterEvent&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-piglet")]
    [CreateAssetMenu(menuName = "Unity Atoms/Value Lists/RumbleParameter", fileName = "RumbleParameterValueList")]
    public sealed class RumbleParameterValueList : AtomValueList<RumbleParameter, RumbleParameterEvent> { }
}
