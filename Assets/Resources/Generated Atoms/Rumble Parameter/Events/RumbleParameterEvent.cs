using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `RumbleParameter`. Inherits from `AtomEvent&lt;RumbleParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/RumbleParameter", fileName = "RumbleParameterEvent")]
    public sealed class RumbleParameterEvent : AtomEvent<RumbleParameter>
    {
    }
}
