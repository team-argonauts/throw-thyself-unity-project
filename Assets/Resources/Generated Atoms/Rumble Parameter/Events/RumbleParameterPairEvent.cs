using UnityEngine;

namespace UnityAtoms
{
    /// <summary>
    /// Event of type `RumbleParameterPair`. Inherits from `AtomEvent&lt;RumbleParameterPair&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-cherry")]
    [CreateAssetMenu(menuName = "Unity Atoms/Events/RumbleParameterPair", fileName = "RumbleParameterPairEvent")]
    public sealed class RumbleParameterPairEvent : AtomEvent<RumbleParameterPair>
    {
    }
}
