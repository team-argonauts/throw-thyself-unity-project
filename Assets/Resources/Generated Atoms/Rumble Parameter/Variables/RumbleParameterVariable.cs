using UnityEngine;
using System;

namespace UnityAtoms
{
    /// <summary>
    /// Variable of type `RumbleParameter`. Inherits from `AtomVariable&lt;RumbleParameter, RumbleParameterPair, RumbleParameterEvent, RumbleParameterPairEvent, RumbleParameterRumbleParameterFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-lush")]
    [CreateAssetMenu(menuName = "Unity Atoms/Variables/RumbleParameter", fileName = "RumbleParameterVariable")]
    public sealed class RumbleParameterVariable : AtomVariable<RumbleParameter, RumbleParameterPair, RumbleParameterEvent, RumbleParameterPairEvent, RumbleParameterRumbleParameterFunction>
    {
        protected override bool ValueEquals(RumbleParameter other)
        {
            if (other.LowA != Value.LowA)
                return false;
            if (other.HighA != Value.HighA)
                return false;

            return true;
        }
    }
}
