namespace UnityAtoms
{
    /// <summary>
    /// Function x 2 of type `RumbleParameter`. Inherits from `AtomFunction&lt;RumbleParameter, RumbleParameter&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-sand")]
    public abstract class RumbleParameterRumbleParameterFunction : AtomFunction<RumbleParameter, RumbleParameter> { }
}
