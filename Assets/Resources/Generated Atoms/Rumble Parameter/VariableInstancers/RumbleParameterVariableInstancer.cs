using UnityEngine;
using UnityAtoms.BaseAtoms;

namespace UnityAtoms
{
    /// <summary>
    /// Variable Instancer of type `RumbleParameter`. Inherits from `AtomVariableInstancer&lt;RumbleParameterVariable, RumbleParameterPair, RumbleParameter, RumbleParameterEvent, RumbleParameterPairEvent, RumbleParameterRumbleParameterFunction&gt;`.
    /// </summary>
    [EditorIcon("atom-icon-hotpink")]
    [AddComponentMenu("Unity Atoms/Variable Instancers/RumbleParameter Variable Instancer")]
    public class RumbleParameterVariableInstancer : AtomVariableInstancer<
        RumbleParameterVariable,
        RumbleParameterPair,
        RumbleParameter,
        RumbleParameterEvent,
        RumbleParameterPairEvent,
        RumbleParameterRumbleParameterFunction>
    { }
}
